package com.stampsure.stampsure;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.stampsure.stampsure.beans.RotateImage;
import com.stampsure.stampsure.fragments.AddItemFragment;
import com.stampsure.stampsure.fragments.CaptureFragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CaptureActivity extends AppCompatActivity {

    private String mParam1;
    private String mParam2;
    private CaptureFragment.OnFragmentInteractionListener mListener;

    private ImageView id_item_image_1;
    private ImageView id_item_image_2;
    private ImageView id_item_image_3;
    private ImageView id_item_image_4;

    private ImageView id_refresh_1;
    private ImageView id_refresh_2;
    private ImageView id_refresh_3;
    private ImageView id_refresh_4;

    private ImageView buttonInvoice;
    private ImageView button_add_photo;
    private Button id_update_item;

    private String mImageFileLocation = "";
    private String imageFileLocation1 = "";
    private String imageFileLocation2 = "";
    private String imageFileLocation3 = "";
    private String imageFileLocation4 = "";
    public String imageFileName = "";
    private int resultCanceled = 0;

    private ArrayList<String> imagePath = new ArrayList<String>();


    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    private static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_INVOICE = 1883;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        id_item_image_1 = (ImageView)findViewById(R.id.id_item_image_1);
        id_item_image_2 = (ImageView)findViewById(R.id.id_item_image_2);
        id_item_image_3 = (ImageView)findViewById(R.id.id_item_image_3);
        id_item_image_4 = (ImageView)findViewById(R.id.id_item_image_4);

        id_refresh_1 = (ImageView)findViewById(R.id.id_refresh_1);
        id_refresh_2 = (ImageView)findViewById(R.id.id_refresh_2);
        id_refresh_3 = (ImageView)findViewById(R.id.id_refresh_3);
        id_refresh_4 = (ImageView)findViewById(R.id.id_refresh_4);


        button_add_photo  = (ImageView)findViewById(R.id.button_add_photo);
        id_update_item  = (Button)findViewById(R.id.id_update_item);
        buttonInvoice   = (ImageView)findViewById(R.id.id_buttonInvoice);
        // buttonInvoice.setVisibility(view.GONE);
        buttonInvoice.setEnabled(false);


        button_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = getImageRequestCode(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        buttonInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);   // From the old
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 2, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_refresh_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);   // From the old
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        id_refresh_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);   // From the old
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        id_refresh_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);   // From the old
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        id_refresh_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);   // From the old
                File photoFile = null;
                try {
                    photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_update_item.setOnClickListener(new View.OnClickListener() {
            @TargetApi(23)
            @Override
            public void onClick(View v) {

                if(imageFileLocation1.isEmpty() && imageFileLocation2.isEmpty() && imageFileLocation3.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Take at least 1 picture", Toast.LENGTH_SHORT).show();
                }else {
                    if (!imageFileLocation1.isEmpty()) {
                        imagePath.add(imageFileLocation1);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation2.isEmpty()) {
                        imagePath.add(imageFileLocation2);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation3.isEmpty()) {
                        imagePath.add(imageFileLocation3);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation4.isEmpty()) {
                        imagePath.add(imageFileLocation4);
                    }else{
                        imagePath.add("");
                    }

                }
            }
        });

    }

    @TargetApi(23)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);
        if(!(resultCode == 0)) {
            resultCanceled = 0;
            if (requestCode == 1880) {
                imageFileLocation1 = mImageFileLocation;
                id_item_image_1.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getApplicationContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_1.setVisibility(View.VISIBLE);
            }

            if (requestCode == 1881) {
                imageFileLocation2 = mImageFileLocation;
                id_item_image_2.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getApplicationContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_2.setVisibility(View.VISIBLE);
            }
            if (requestCode == 1882) {
                imageFileLocation3 = mImageFileLocation;
                id_item_image_3.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getApplicationContext(), getResources(), mImageFileLocation, 97, 90));
                button_add_photo.setEnabled(false);
                buttonInvoice.setEnabled(true);
                id_refresh_3.setVisibility(View.VISIBLE);
            }
            if (requestCode == 1883) {
                imageFileLocation4 = mImageFileLocation;
                id_item_image_4.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getApplicationContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_4.setVisibility(View.VISIBLE);
            }
        }else{
            resultCanceled = 1;
        }
    }

    private int getImageRequestCode(int request_code){
        int IMAGE_REQUEST_CODE = 0;
        switch (request_code){
            case 0:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1880;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1880:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1881;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1881:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1882;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1882:
                IMAGE_REQUEST_CODE = request_code;
                break;
            default:
                IMAGE_REQUEST_CODE = 1880;
        }
        return IMAGE_REQUEST_CODE;
    }

    private File createPictureFile(String CategoryName, int itemId, int pictureId) throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (itemId == 1) {
            imageFileName = "Temp" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }else if(itemId == 2){
            imageFileName = "Invoice" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }
        File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),CategoryName);

        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        Log.d("CaptureFragment",fileStorageDirectory.toString());
        File image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        return image;
    }
}
