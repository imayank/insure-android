package com.stampsure.stampsure.Registration;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.StampActivity;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.RequestPermissionHandler;
import com.stampsure.stampsure.beans.RotateImage;
import com.stampsure.stampsure.beans.UserProfile;

import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class UpdateProfile extends AppCompatActivity {

    private static final int GALLERY_REQUEST = 1880;
    private static final int CAMERA_REQUEST = 1881;
    private static final String TAG = "UpdateProfile";
    private String id_selected = "";
    public String imageFileName = "";
    public String mImageFileLocation = "";
    private RequestPermissionHandler requestPermissionHandler;
    private File image;
    private CircleImageView circleImageView;

    private TextView id_update_profile;

    private EditText id_name;
    private EditText id_lastName;
    private EditText id_email;
    private Spinner id_passport_type;
    private EditText id_number ;
    private EditText id_res_address;
    private EditText id_post_address;
    private EditText id_cell;
    private EditText id_date_Of_birth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.id_action_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Update Profile");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        circleImageView = (CircleImageView)findViewById(R.id.circleImageView);
        TextView id_update_profile = findViewById(R.id.id_update_profile);

        EditText id_name = findViewById(R.id.id_name);
        EditText id_lastName = findViewById(R.id.id_lastName);
        EditText id_email = findViewById(R.id.id_email);
        Spinner id_passport_type = (Spinner)findViewById(R.id.id_passport_type);
        EditText id_number  = (EditText)findViewById(R.id.id_number);
        EditText id_res_address = (EditText)findViewById(R.id.id_res_address);
        EditText id_post_address = (EditText)findViewById(R.id.id_post_address);
        EditText id_cell    = (EditText)findViewById(R.id.id_cell);
        EditText id_date_Of_birth = (EditText)findViewById(R.id.id_date_Of_birth);

        id_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(UpdateProfile.this);
                createOptionDialog.setTitle("Select Option");
                createOptionDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent,GALLERY_REQUEST);
                    }
                });
                createOptionDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        File photoFile = null;
                        try {
                            photoFile = createPictureFile("Profile_Picture", CAMERA_REQUEST);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                });
                createOptionDialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.update_profile_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.id_update_profile:
                validateProfile();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case GALLERY_REQUEST:
                    try {
                        Uri imageUri = data.getData();
                        InputStream inputStream = getContentResolver().openInputStream(imageUri);
                        Bitmap selectedImage = BitmapFactory.decodeStream(inputStream) ;

                        Glide.with(getApplicationContext())
                                .asBitmap()
                                .load(selectedImage)
                                .into(circleImageView);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(UpdateProfile.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                    break;
                case CAMERA_REQUEST:
                    try {
                        Uri imageUri = Uri.fromFile(new File(mImageFileLocation));
                        InputStream inputStream = getContentResolver().openInputStream(imageUri);

                        Bitmap selectedImage = RotateImage.decodeSampledBitmapFromResource(getApplicationContext(),getResources(),mImageFileLocation,97,90);

                        Glide.with(getApplicationContext())
                                .asBitmap()
                                .load(selectedImage)
                                .into(circleImageView);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(UpdateProfile.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    break;
            }
        }
    }
    private File createPictureFile(String userProfile, int pictureId) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        imageFileName = userProfile + "_" + pictureId + "_" + timeStamp + "_";
        final File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),userProfile);
        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        Log.d(TAG,fileStorageDirectory.toString());
        requestPermissionHandler = new RequestPermissionHandler();

        requestPermissionHandler.requestPermission(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123,
                new RequestPermissionHandler.RequestPermissionListener() {
                    @Override
                    public void onSuccess() {
                        try{
                            image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
                            mImageFileLocation = image.getAbsolutePath();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailed() {
                        Toast.makeText(getApplicationContext(),"Access denied to create image",Toast.LENGTH_SHORT).show();
                    }
                });
        return image;
                }

    public void validateProfile(){

        EditText id_name = findViewById(R.id.id_name);
        EditText id_lastName = findViewById(R.id.id_lastName);
        EditText id_email = findViewById(R.id.id_email);
        Spinner id_passport_type = (Spinner)findViewById(R.id.id_passport_type);
        EditText id_number  = (EditText)findViewById(R.id.id_number);
        EditText id_res_address = (EditText)findViewById(R.id.id_res_address);
        EditText id_post_address = (EditText)findViewById(R.id.id_post_address);
        EditText id_cell    = (EditText)findViewById(R.id.id_cell);
        EditText id_date_Of_birth = (EditText)findViewById(R.id.id_date_Of_birth);

        final UserProfile profile = new UserProfile();

        if(!TextUtils.isEmpty(id_name.getText().toString())){
            profile.setfName(id_name.getText().toString());
        }else{
            id_name.setError("Invalid First Name");
            id_name.requestFocus();
        }

        if(!TextUtils.isEmpty(id_lastName.getText().toString())){
            profile.setLname(id_lastName.getText().toString());
        }else{
            id_lastName.setError("Invalid Last Name");
            id_lastName.requestFocus();
        }

        if(!TextUtils.isEmpty(id_email.getText().toString()) && android.util.Patterns.EMAIL_ADDRESS.matcher(id_email.getText().toString()).matches()){
            profile.setEmailId(id_email.getText().toString());
        }else{
            id_email.setError("Invalid Email Id");
            id_email.requestFocus();
        }

        if(!TextUtils.isEmpty(id_cell.getText().toString()) && isValidNumber(id_number.getText().toString())){
            profile.setPhoneNo(Integer.parseInt(id_cell.getText().toString()));
        }else{
            id_cell.setError("Invalid Phone Number");
            id_cell.requestFocus();
        }
        profile.setIdNumber(id_number.getText().toString());          // Natioanl Id/ Passport/ Refugee number input
        profile.setEntityId(Integer.parseInt("1"));                // 1 stands for Client type Individual
        profile.setIdType(id_selected);                               // ID type Natioanl Id/ Passport/ Refugee

        id_passport_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                profile.setIdType(parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        profile.setDob(null);       // id_date_Of_birth
        profile.setAddress1("");       // id_res_address
        profile.setAddress2("");        // id_pos_address
        new HttpUpdateUserTask().execute(profile);

        // return profile;
    }
    public boolean isValidNumber(String s){
        for(char c : s.toCharArray()){
            if(Character.isDigit(c)){
                return true;
            }
        }
        return false;
    }

    private class HttpUpdateUserTask extends AsyncTask<UserProfile, Void, Boolean> {
        @Override
        protected Boolean doInBackground(UserProfile... params) {
            Boolean response = false;

            try {
         //       final String updateProfileURL = "http://localhost:8090/profile/updateUserProfile";
                final String updateProfileURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/updateUserProfile";
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                response = restTemplate.postForObject(updateProfileURL, params[0], Boolean.class);
            } catch (Exception e) {
                ComponentBuilder.hideProgressDialogue();
                Log.e(TAG, e.getMessage(), e);
            }
            return response;
        }

        @Override
        protected void onPreExecute() {
            // ComponentBuilder.showProgressDialogue(getApplicationContext(),"Uploading Images", "Please wait for a moment.");
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success){
                Toast.makeText(getApplicationContext(), "Registration Success on REST", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), "Registration Not Success on REST", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
