
package com.stampsure.stampsure.Registration;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;

import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.CategoryList;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.ExifBitmap;
import com.stampsure.stampsure.beans.RequestPermissionHandler;
import com.stampsure.stampsure.beans.RotateImage;
import com.stampsure.stampsure.beans.URLComponent;
import com.stampsure.stampsure.beans.UserProfile;
import com.stampsure.stampsure.StampActivity;

import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;


import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class Register extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText emailAddress;
    private EditText phoneNo;
    private EditText idNumber;
    private EditText passwordText;

    private ImageView id_update_profile;
    private CircleImageView circleImageView;
    private Spinner id_user_category;
    private Button id_register_button;
    private byte[] profilePicture;
    private static final int GALLERY_REQUEST = 1880;
    private static final int CAMERA_REQUEST = 1881;
    private static final String TAG = "Register";

    private String id_type_selected = "";
    private String imageFileName = "";
    private String mImageFileLocation = "";
    private RequestPermissionHandler requestPermissionHandler;

    private File image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getFormObjects();

        id_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(Register.this);
                createOptionDialog.setTitle("Select Option");
                createOptionDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                    }
                });
                createOptionDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        File photoFile = null;
                        try {
                            photoFile = createPictureFile("Profile_Picture", CAMERA_REQUEST);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(photoFile != null) {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                    }
                });
                createOptionDialog.show();
            }
        });

        // Select User Id type, National ID, Passport....

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.user_id_category, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_user_category.setAdapter(spinnerAdapter);

        id_user_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_type_selected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        id_register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(id_register_button.getWindowToken(), 0);

           //     getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                UserProfile profile = new UserProfile();

                if(isValidFirstName(firstName)){
                    profile.setfName(firstName.getText().toString());
                }
                if(isValidLastName(lastName)){
                    profile.setLname(lastName.getText().toString());
                }
                if(isValidEmail(emailAddress)){
                    profile.setEmailId(emailAddress.getText().toString());
                }
                if(isValidPhoneNumber(phoneNo)){
                    profile.setPhoneNo(Integer.parseInt(phoneNo.getText().toString()));
                }

                if(isValidPassword(passwordText)){
                    profile.setLogonPassword(passwordText.getText().toString());
                }

                if (!firstName.getText().toString().isEmpty() &&
                        !emailAddress.getText().toString().isEmpty() &&
                        !idNumber.getText().toString().isEmpty() &&
                        !phoneNo.getText().toString().isEmpty()) {

                    profile.setIdType(id_type_selected);
                  //  profile.setLogonPassword(passwordText.getText().toString());
                    profile.setIdNumber(idNumber.getText().toString());
                    profile.setProfilePicture(profilePicture);
                    // Defaults
                    profile.setAddress1("");
                    profile.setAddress2("");
                    profile.setRegistrationDate(new Date());
                    profile.setPostalCode(Integer.parseInt("0000"));
                    profile.setCountryId(Integer.parseInt("00"));
                    profile.setDob(new Date());
                    profile.setEntityId(Integer.parseInt("1"));

                    new HttpAddUserTask().execute(profile);

                } else{
                    Toast.makeText(getApplicationContext(),"Incomplete Details",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getFormObjects() {
        Toolbar toolbar = findViewById(R.id.id_action_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register");
        firstName = findViewById(R.id.id_name);
        lastName = findViewById(R.id.id_lastName);
        emailAddress = findViewById(R.id.id_email);
        phoneNo = findViewById(R.id.phoneNo);
        idNumber = findViewById(R.id.id_number);
        passwordText = findViewById(R.id.id_password);
        id_update_profile = findViewById(R.id.id_update_profile);
        circleImageView = findViewById(R.id.circleImageView);
        id_user_category = findViewById(R.id.id_user_id_category);
        id_register_button = findViewById(R.id.id_register_button);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case GALLERY_REQUEST:
                    try {
                        Uri imageUri = data.getData();
                        InputStream inputStream = getContentResolver().openInputStream( imageUri);

                        Bitmap selectedImage = BitmapFactory.decodeStream(inputStream);
                        profilePicture = ComponentBuilder.getBytesFromBitmap(selectedImage);
                        Glide.with(getApplicationContext())
                                .asBitmap()
                                .load(selectedImage)
                                .into(circleImageView);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(Register.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                    break;
                case CAMERA_REQUEST:
                        Bitmap selectedImage = ExifBitmap.rotateBitmap(mImageFileLocation,BitmapFactory.decodeFile(mImageFileLocation));
                       // Bitmap selectedImage = RotateImage.decodeSampledBitmapFromResource(getApplicationContext(),getResources(),mImageFileLocation,97,90);
                         profilePicture = ComponentBuilder.getCompressedFile(getApplicationContext(),mImageFileLocation);
                        Glide.with(getApplicationContext())
                                .asBitmap()
                                .load(selectedImage)
                                .into(circleImageView);
                        break;
                default:
                        break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.register_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.id_register:
                UserProfile profile = new UserProfile();

            if(isValidFirstName(firstName)){
                profile.setfName(firstName.getText().toString());
            }
            if(isValidLastName(lastName)){
                profile.setLname(lastName.getText().toString());
            }
            if(isValidEmail(emailAddress)){
                profile.setEmailId(emailAddress.getText().toString());
            }
            if(isValidPhoneNumber(phoneNo)){
                profile.setPhoneNo(Integer.parseInt(phoneNo.getText().toString()));
            }
            if(isValidPassword(passwordText)){
                profile.setLogonPassword(passwordText.getText().toString());
            }
             if (!firstName.getText().toString().isEmpty() ||
                    !emailAddress.getText().toString().isEmpty() ||
                    !idNumber.getText().toString().isEmpty() ||
                    !phoneNo.getText().toString().isEmpty()) {

                    profile.setIdType(id_type_selected);
                  //  profile.setLogonPassword(passwordText.getText().toString());
                    profile.setIdNumber(idNumber.getText().toString());
                    profile.setProfilePicture(profilePicture);

                    // Defaults
                    profile.setAddress1("");
                    profile.setAddress2("");
                    profile.setRegistrationDate(new Date());
                    profile.setPostalCode(Integer.parseInt("0000"));
                    profile.setCountryId(Integer.parseInt("00"));
                    profile.setDob(new Date());
                    profile.setEntityId(Integer.parseInt("1"));

                     new HttpAddUserTask().execute(profile);

                } else{
                    Toast.makeText(getApplicationContext(),"Incomplete Details",Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private File createPictureFile(String userProfileName, int pictureId) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        imageFileName = userProfileName + "_" + pictureId + "_" + timeStamp + "_";
        final File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),userProfileName);
        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        requestPermissionHandler = new RequestPermissionHandler();

        requestPermissionHandler.requestPermission(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123,
                new RequestPermissionHandler.RequestPermissionListener() {
                    @Override
                    public void onSuccess() {
                        try{
                            image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
                            mImageFileLocation = image.getAbsolutePath();

                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailed() {
                        Toast.makeText(getApplicationContext(),"Access denied to create image",Toast.LENGTH_SHORT).show();
                    }
                });
        if(image != null) {
            return image;
        }
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        requestPermissionHandler.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }


    private class HttpAddUserTask extends AsyncTask<UserProfile, Void, UserProfile> {
        @Override
        protected UserProfile doInBackground(UserProfile... params) {
            UserProfile response = new UserProfile();
            try {
            //    final String addProfileURL = "http://10.0.75.1:8090/profile/addUserProfile";
            //    final String addProfileURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/addUserProfile";
                final String addProfileURL = URLComponent.addProfileURL;
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                response = restTemplate.postForObject(addProfileURL, params[0], UserProfile.class);
            } catch (Exception e) {
                ComponentBuilder.hideProgressDialogue();
                Log.e(TAG, e.getMessage(), e);
            }
            return response;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(), "Registration in progress...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(UserProfile userProfile) {

            if (userProfile != null && userProfile.getEmailId() != null && userProfile.getEmailId().length() > 0) {
                Toast.makeText(getApplicationContext(), "Registration successful...", Toast.LENGTH_SHORT).show();

                new HttpGetCategories().execute();

                ComponentBuilder.setUIDFromActivity(getApplicationContext(), userProfile.getId());

                Bundle bundle = new Bundle();
                bundle.putString("name",userProfile.getfName());
                bundle.putString("email",userProfile.getEmailId());
                bundle.putByteArray("profile_picture",userProfile.getProfilePicture());

                Intent stampIntent = new Intent(getApplicationContext(), StampActivity.class);
                stampIntent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                stampIntent.putExtras(bundle);

                startActivity(stampIntent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private class HttpGetCategories extends AsyncTask<Void,Void,ArrayList<CategoryList>> {
        @Override
        protected ArrayList<CategoryList> doInBackground(Void... voids) {
            ArrayList<CategoryList> categoryLists  = new ArrayList<>();
            try{
            //    CategoryList[] forNow = ComponentBuilder.getRestTemplate().getForObject("http://10.0.75.1:8090/profile/categories", CategoryList[].class);
            //    CategoryList[] forNow = ComponentBuilder.getRestTemplate().getForObject("http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/categories", CategoryList[].class);
                CategoryList[] forNow = ComponentBuilder.getRestTemplate().getForObject(URLComponent.getCategoryURL, CategoryList[].class);
                categoryLists = new ArrayList(Arrays.asList(forNow));
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),"Couldn't get the categories",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            return  categoryLists;
        }
        @Override
        protected void onPostExecute(ArrayList<CategoryList> categoryLists) {
            ArrayList<String> clist = new ArrayList<String>();
            for(int i = 0; i < categoryLists.size(); i++){
                clist.add(categoryLists.get(i).getCategoryName());
            }
            Set<String> cateList = new HashSet<String>(clist);
            ComponentBuilder.setCategoryFromActivity(getApplicationContext(),cateList);
            LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(getApplicationContext());
            boolean result =  dbScreenUserHandler.putCategory(categoryLists);
            if(result){
                Toast.makeText(getApplicationContext(),"Categories updated successfully" ,Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(),"Categories updated failed" ,Toast.LENGTH_LONG).show();
            }
        }
        @Override
        protected void onPreExecute() {
          //  Toast.makeText(getApplicationContext(),"Fetching Categories..." ,Toast.LENGTH_LONG).show();
        }
    }

    public static boolean isValidFirstName(EditText inputFirstName){
        if(!TextUtils.isEmpty(inputFirstName.getText().toString())){
            return true;
        }else{
            inputFirstName.setError("Invalid First Name");
            inputFirstName.requestFocus();
        }
        return false;
    }

    public static boolean isValidLastName(EditText inputLastName){
        if(!TextUtils.isEmpty(inputLastName.getText().toString())){
            return true;
        }else{
            inputLastName.setError("Invalid last Name");
            inputLastName.requestFocus();
        }
        return false;
    }

    public static boolean isValidEmail(EditText inputEmail){
        if(!TextUtils.isEmpty(inputEmail.getText().toString()) && android.util.Patterns.EMAIL_ADDRESS.matcher(inputEmail.getText().toString()).matches()){
            return true;
        }else{
            inputEmail.setError("Invalid last Name");
            inputEmail.requestFocus();
        }
        return false;
    }

    public static boolean isValidPhoneNumber(EditText phoneNo){
        String s = phoneNo.getText().toString();
        if(TextUtils.isEmpty(s)){
            phoneNo.setError("Invalid Phone number");
            phoneNo.requestFocus();
        }

        for (char c : s.toCharArray()){
            if(!s.startsWith("0")){
                phoneNo.setError("First Digit should be zero");
                phoneNo.requestFocus();
            }
            if(Character.isDigit(c)){
                return true ;
            }else{
                phoneNo.setError("Invalid phone number");
                phoneNo.requestFocus();
            }
        }
        return false;
    }

    public static boolean isValidPassword(EditText passwordText){

        if(!TextUtils.isEmpty(passwordText.getText().toString())){
            String s = passwordText.getText().toString();
            char ch;
            boolean isLetter = false;
            boolean oneCapLetter = false;
            boolean oneNumber = false;
            boolean minLength = false;
            int len = 0;
            for(int i=0; i < s.length(); i++){
               ch = s.charAt(i);
               if(Character.isDigit(ch)){
                   oneNumber = true;
               }else if(Character.isUpperCase(ch)){
                   oneCapLetter = true;
               }else if(Character.isLetter(ch)){
                   isLetter = true;
               }
            }
            if(s.length() >= 6){
                minLength = true;
            }
            if(oneNumber && oneCapLetter && minLength && isLetter ) {
                return true;
            }else{
                passwordText.setError("Combination of atleast one letter, one capital letter, one number and 6 characters");
                passwordText.requestFocus();
            }
        }else{
            passwordText.setError("Combination of atleast one letter, one capital letter, one number and 6 characters");
            passwordText.requestFocus();
        }
        return false;
    }
}
