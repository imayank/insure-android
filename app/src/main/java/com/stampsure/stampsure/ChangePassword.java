package com.stampsure.stampsure;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.ForgotPasswordProfile;
import com.stampsure.stampsure.beans.URLComponent;
import com.stampsure.stampsure.beans.UserProfile;

import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

public class ChangePassword extends AppCompatActivity {
    private static final String TAG = "ChangePassword";
    private HttpForgotPassword mForgotPsw = null;

    private TextView id_message;
    private Button id_button_continue  ;
    private EditText id_temp_password ;
    private EditText id_new_password ;
    private Button id_button_reset_password ;
    private TextView id_change_password_user_name ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.id_action_bar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle(R.string.change_psw);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id_message = (TextView) findViewById(R.id.id_message);
        id_button_continue = (Button) findViewById(R.id.id_continue);
        id_temp_password = (EditText) findViewById(R.id.id_temp_password);
        id_new_password = (EditText) findViewById(R.id.id_new_password);
        id_button_reset_password = (Button) findViewById(R.id.id_button_reset_password);
        id_change_password_user_name = (TextView) findViewById(R.id.id_change_password_user_name);

        id_message.setVisibility(View.GONE);
        id_button_continue.setVisibility(View.GONE);

        id_change_password_user_name.setText(ComponentBuilder.getLoggedInUserNameToPref(getApplicationContext()));
        id_button_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserProfile forgotPassword = new UserProfile();

                forgotPassword.setEmailId(ComponentBuilder.getLoggedInUserEmailFromActivity(getApplicationContext()));
                forgotPassword.setTempPassword(id_temp_password.getText().toString());
                forgotPassword.setLogonPassword(id_new_password.getText().toString());

                if(forgotPassword.getEmailId().isEmpty() && forgotPassword.getTempPassword().isEmpty()){
                    mForgotPsw = new HttpForgotPassword(ChangePassword.this);
                    mForgotPsw.execute(forgotPassword);
                }else{
                    Toast.makeText(getApplicationContext(),"Please put details",Toast.LENGTH_SHORT).show();
                }


            }
        });

        id_button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent stampIntent = new Intent(ChangePassword.this, StampActivity.class);
                startActivity(stampIntent);
            }
        });
    }
    private static class HttpForgotPassword extends AsyncTask<UserProfile, Void, Boolean>{
        private WeakReference<ChangePassword> weakReference;
        private ProgressDialog progressDialog;
        HttpForgotPassword (ChangePassword context){
            weakReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            ChangePassword changePassword = weakReference.get();
            super.onPreExecute();
            progressDialog = new ProgressDialog(changePassword);
            progressDialog.setMessage("Changing password");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(UserProfile... forgotPasswordProfiles) {
            Boolean response = false;
            try{
                String connectString = URLComponent.changePasswordURL;;
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                response = restTemplate.postForObject(connectString, forgotPasswordProfiles[0], Boolean.class);
            }catch (Exception e){
                Log.d(TAG, "doInBackground: "+ e);
            }
            return response;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            ChangePassword changePassword = weakReference.get();
            super.onPostExecute(aBoolean);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(aBoolean){
                changePassword.id_new_password.setEnabled(false);
                changePassword.id_temp_password.setEnabled(false);
                changePassword.id_button_reset_password.setEnabled(false);
                changePassword.id_message.setVisibility(View.VISIBLE);
                changePassword.id_button_continue.setVisibility(View.VISIBLE);

                changePassword.id_message.setText(R.string.psw_change_message);
            }else{
                changePassword.id_message.setVisibility(View.VISIBLE);
                changePassword.id_message.setText(R.string.psw_change_unsuccess);
            }
        }
    }

}
