package com.stampsure.stampsure;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.Registration.Register;
import com.stampsure.stampsure.beans.CategoryList;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.URLComponent;
import com.stampsure.stampsure.beans.UserProfile;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;
import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final int WRITE_EXTERNAL_STORAGE_PIC = 0;
    private static final String TAG = "LoginActivity";

    private HttpLoginActivity mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final TextInputLayout id_email_layout = findViewById(R.id.id_email_layout);
        final LinearLayout id_already_registered = findViewById(R.id.id_already_registered);
        final TextView id_diff_user = findViewById(R.id.id_diff_user);
        TextView id_user_name = findViewById(R.id.id_user_name);
        mEmailView = findViewById(R.id.email);

        // Get the user details from the shared preferences.

        String loggedInUserEmail = ComponentBuilder.getLoggedInUserEmailFromActivity(getApplicationContext());
        String loggedInUser = ComponentBuilder.getLoggedInUserNameToPref(getApplicationContext());
        Log.d(TAG, "User Email Stored " + loggedInUserEmail + " User Name " + loggedInUser);

        if (!loggedInUserEmail.isEmpty()) {
            id_email_layout.setVisibility(View.GONE);
            id_already_registered.setVisibility(View.VISIBLE);
            id_user_name.setText(loggedInUser);
            mEmailView.setText(loggedInUserEmail);
        } else {
            id_diff_user.setVisibility(View.GONE);
            populateAutoComplete();
        }

    //    mayRequestWritePermission();
        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    ComponentBuilder.setLoggedInUserEmailFromActivity(getApplicationContext(), mPasswordView.getText().toString());
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mEmailView.getWindowToken(), 0);
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        final Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ComponentBuilder.setLoggedInUserEmailFromActivity(getApplicationContext(), mEmailView.getText().toString());
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mEmailSignInButton.getWindowToken(), 0);

                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        TextView id_register_link = findViewById(R.id.id_register_link);


        id_register_link.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, Register.class);
                startActivity(registerIntent);
            }
        });

        id_diff_user.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ComponentBuilder.setLoggedInUserEmailFromActivity(getApplicationContext(), "");
                ComponentBuilder.setLoggedInUserNameToPref(getApplicationContext(), "");
                id_already_registered.setVisibility(View.GONE);
                id_email_layout.setVisibility(View.VISIBLE);
                id_diff_user.setVisibility(View.GONE);

            }
        });

        TextView id_forgot_psw = findViewById(R.id.id_forgot_psw);
        id_forgot_psw.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgotPassword = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(forgotPassword);
            }
        });
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        if(!mayRequestWritePermission()){
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);

        }
        return false;
    }

    private boolean mayRequestWritePermission(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PIC);
        return false;
    }
    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS ) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            UserProfile profile = new UserProfile();
            profile.setEmailId(email);
            profile.setLogonPassword(password);

            ConnectivityManager connMgr = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {
                mAuthTask = new HttpLoginActivity(LoginActivity.this);
                mAuthTask.execute(profile);
            }else{
                Toast.makeText(getApplicationContext(),"Please check network connection",Toast.LENGTH_SHORT).show();
                showProgress(false);
            }

        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private static class HttpLoginActivity extends AsyncTask<UserProfile, Void, UserProfile> {
        private WeakReference<LoginActivity> weakReference;

        HttpLoginActivity (LoginActivity context){
            weakReference = new WeakReference<>(context);
        }

        @Override
        protected UserProfile doInBackground(UserProfile... params) {
            LoginActivity activity = weakReference.get();
            UserProfile profile = new UserProfile();
            try {
                final String loginURL = URLComponent.loginURL;
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                profile = restTemplate.postForObject(loginURL, params[0], UserProfile.class);
            } catch (ResourceAccessException e) {
                Log.d(TAG,"Exception "+ e);
            }catch (Exception e){
                Log.d(TAG,"Exception "+ e);
            }
            return profile;
        }
        @Override
        protected void onPostExecute(UserProfile profile) {
            LoginActivity activity = weakReference.get();
            if (profile.getEmailId() == null) {
                activity.mAuthTask = null;
                activity.mPasswordView.setError(activity.getString(R.string.error_incorrect_password));
                activity.mPasswordView.requestFocus();
                activity.showProgress(false);
                Toast.makeText(activity,"Cannot login ",Toast.LENGTH_SHORT).show();
            } else {
                LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(activity);
                databaseHandler.deleteExistingProfiles();
                databaseHandler.insertProfileForLoginScreen(profile.getfName(),profile.getId(),profile.getEmailId(),profile.getProfilePicture());

                ComponentBuilder.setLoggedInUserEmailFromActivity(activity, profile.getEmailId());
                ComponentBuilder.setLoggedInUserNameToPref(activity, profile.getfName());
                ComponentBuilder.setUIDFromActivity(activity, profile.getId());

                if(profile.isAuthenticated() && profile.isTempPswd()){
                    Intent changePassword = new Intent(activity, ChangePassword.class);
                    activity.startActivity(changePassword);
                }else{
                    Intent stampIntent = new Intent(activity, StampActivity.class);
                    activity.startActivity(stampIntent);
                }

                activity.mAuthTask = null;
                activity.showProgress(false);
            }
        }
        @Override
        protected void onCancelled() {
            LoginActivity activity = weakReference.get();
            activity.mAuthTask = null;
            activity.showProgress(false);
        }
    }
}

