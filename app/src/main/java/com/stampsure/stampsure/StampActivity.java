package com.stampsure.stampsure;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.BottomNavigationViewHelper;
import com.stampsure.stampsure.beans.CategoryList;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.URLComponent;
import com.stampsure.stampsure.beans.UserProfile;
import com.stampsure.stampsure.fragments.CaptureFragment;
import com.stampsure.stampsure.fragments.AddItemFragment;
import com.stampsure.stampsure.fragments.DashBoardCategory;
import com.stampsure.stampsure.fragments.DashboardAge;
import com.stampsure.stampsure.fragments.DashboardLocation;
import com.stampsure.stampsure.fragments.DashboardManager;
import com.stampsure.stampsure.fragments.Help;
import com.stampsure.stampsure.fragments.ItemListFragment;
import com.stampsure.stampsure.fragments.Notification;
import com.stampsure.stampsure.fragments.UidDashboard;
import com.stampsure.stampsure.fragments.UpdateItemFragment;
import com.stampsure.stampsure.fragments.UpdateServerItemFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class StampActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   CaptureFragment.OnFragmentInteractionListener,
                    AddItemFragment.OnFragmentInteractionListener,
                    ItemListFragment.OnFragmentInteractionListener,
                    Notification.OnFragmentInteractionListener,
                    Help.OnFragmentInteractionListener,
                    UpdateItemFragment.OnFragmentInteractionListener,
                    DashBoardCategory.OnFragmentInteractionListener,
                    DashboardManager.OnFragmentInteractionListener,
                    DashboardLocation.OnFragmentInteractionListener,
                    DashboardAge.OnFragmentInteractionListener,
                    UidDashboard.OnFragmentInteractionListener,
                    UpdateServerItemFragment.OnFragmentInteractionListener{

    private static final String TAG = "StampActivity";
    private static int UID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"StampActivity onCreate started");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stamp);

        Toolbar toolbar =  findViewById(R.id.id_action_bar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Stamp Sure");

        new HttpGetCategories(StampActivity.this).execute();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        TextView id_profile_name = headerView.findViewById(R.id.id_profile_name);
        TextView id_email        = headerView.findViewById(R.id.id_email);
        CircleImageView id_profileImage = headerView.findViewById(R.id.id_profileImage);

        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getApplicationContext());
        UserProfile profile =  databaseHandler.getProfile(ComponentBuilder.getUIDFromActivity(getApplicationContext()));

        if(profile.getEmailId() == null){
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                byte[] profileImage_register = extras.getByteArray("profile_picture");
                if (profileImage_register != null) {
                    Bitmap pf = BitmapFactory.decodeByteArray(profileImage_register, 0, profileImage_register.length);
                    id_profileImage.setImageBitmap(pf);
                } else {
                    id_profileImage.setImageResource(R.drawable.account_icon);
                }
                String email = extras.getString("email");
                String name  = extras.getString("name");

                id_profile_name.setText(name);
                id_email.setText(email);
            }
        }else {
            id_profile_name.setText(profile.getfName());
            id_email.setText(profile.getEmailId());
            if(profile.getProfilePicture() != null) {
                id_profileImage.setImageBitmap(BitmapFactory.decodeByteArray(profile.getProfilePicture(), 0, profile.getProfilePicture().length));
            }else{
                id_profileImage.setImageResource(R.drawable.account_icon);
            }
        }
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView navigation = findViewById(R.id.bottom_navigation_bar);
        BottomNavigationViewHelper.disableShiftMode(navigation);

        callPieChart();

        navigation.getMenu().getItem(0).setChecked(true);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch(item.getItemId()){
                    case R.id.dashboard:
                        callPieChart();
                        return true;
                    case R.id.capture:
                        // Call categories from the database.
                        // new HttpGetCategories(StampActivity.this).execute();
                        CaptureFragment captureFragment = new CaptureFragment();
                        fragmentTransaction.replace(R.id.id_main_frame, captureFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        return true;
                    case R.id.notification:

                        Notification notificationFragment = new Notification();
                        fragmentTransaction.replace(R.id.id_main_frame, notificationFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        return true;
                    case R.id.help:

                        ItemListFragment itemListFragment = new ItemListFragment();
                        fragmentTransaction.replace(R.id.id_main_frame, itemListFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
/*
                        Help helpFragment = new Help();
                        fragmentTransaction.replace(R.id.id_main_frame, helpFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();  */
                        return true;
                    default:
                        break;
                }
                return false;
            }
        });
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
/*
        if (id == R.id.nav_update_profile) {
            // Handle the camera action
            // Intent updateIntent = new Intent(this,UpdateProfile.class);
            // updateIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            // startActivity(updateIntent);

        } else*/
            if (id == R.id.nav_logout) {
            Intent loginIntent = new Intent(this,LoginActivity.class);
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(loginIntent);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private static class HttpGetCategories extends AsyncTask<Void,Void,ArrayList<CategoryList>> {
        private WeakReference<StampActivity> weakReference;
        HttpGetCategories(StampActivity context){
            weakReference = new WeakReference<>(context);
        }
        @Override
        protected ArrayList<CategoryList> doInBackground(Void... voids) {
            ArrayList<CategoryList> categoryLists  = new ArrayList<>();
            try{
                CategoryList[] forNow = ComponentBuilder.getRestTemplate().getForObject(URLComponent.getCategoryURL, CategoryList[].class);
                categoryLists = new ArrayList<>(Arrays.asList(forNow));
            }catch(Exception e){
                Log.d(TAG, "doInBackground: HttpGetCategories - Couldn't get the categories");
                e.printStackTrace();
            }
            return  categoryLists;
        }
        @Override
        protected void onPostExecute(ArrayList<CategoryList> categoryLists) {
            StampActivity activity = weakReference.get();

            LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(activity);
            databaseHandler.deleteCategory();

            ArrayList<String> cList = new ArrayList<>();
            for(int i = 0; i < categoryLists.size(); i++){
                cList.add(categoryLists.get(i).getCategoryName());
            }
            Set<String> cateList = new HashSet<>(cList);
            ComponentBuilder.setCategoryFromActivity(activity,cateList);

            LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(activity);
            boolean result =  dbScreenUserHandler.putCategory(categoryLists);
            if(result){
                Log.d(TAG, "onPostExecute: HttpGetCategories - Category Stored");
            }else{
                Log.d(TAG, "onPostExecute: HttpGetCategories - Category Store Failed");
            }
        }
        @Override
        protected void onPreExecute() {
            Log.d(TAG, "onPreExecute: HttpGetCategories - Fetching categories");
        }
    }

    public void callPieChart(){
        new HttpGetAssetList(StampActivity.this).execute();
    }

    private static class HttpGetAssetList extends AsyncTask<Void,Void,ArrayList<Asset>> {
        private WeakReference<StampActivity> weakReference;
        ArrayList<Asset> assetList = new ArrayList<>();
        HttpGetAssetList(StampActivity context){
            weakReference = new WeakReference<>(context);
        }

       private ProgressDialog progressDialog;
        @Override
        protected ArrayList<Asset> doInBackground(Void... voids) {
            StampActivity activity = weakReference.get();
            UID = ComponentBuilder.getUIDFromActivity(activity);
            String stampURL = URLComponent.getAssetURL + "/" + UID;
            try{
                Asset[] forNow = ComponentBuilder.getRestTemplate().getForObject(stampURL, Asset[].class);
                assetList = new ArrayList<>(Arrays.asList(forNow));
            }catch(Exception e){
                e.printStackTrace();
            }
            return assetList;
        }
        @Override
        protected void onPreExecute() {
            StampActivity activity = weakReference.get();
        //    Toast.makeText(activity,"Fetching the assets", Toast.LENGTH_SHORT).show();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Loading Items");
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(ArrayList<Asset> assets) {
            StampActivity activity = weakReference.get();
            int asset_numbers = 0;
            if(assets.size() != 0) {
                UID = ComponentBuilder.getUIDFromActivity(activity);
                LocalDatabaseHandler deleteHandler = new LocalDatabaseHandler(activity);

                deleteHandler.deleteAssetServerData(0, UID);

                for (int i = 0; i < assets.size(); i++) {
                    LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(activity);
                    databaseHandler.setAssetDataFromServer(assets.get(i));
                    asset_numbers = i;
                    Log.d(TAG, "onPostExecute: Storing " + asset_numbers + " " + assets.get(i).getAssetName());
                }

                if (assets.size() == (asset_numbers + 1)) {

                    FragmentManager pie_fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction pie_fragmentTransaction = pie_fragmentManager.beginTransaction();
                    DashboardManager dashboardFragment = new DashboardManager();
                    pie_fragmentTransaction.replace(R.id.id_main_frame, dashboardFragment);
//
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    pie_fragmentTransaction.addToBackStack(null);
                    pie_fragmentTransaction.commit();
                }
            }else{
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(activity,"No items stored, please capture",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
