package com.stampsure.stampsure.LocalDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.AudioFocusRequest;
import android.nfc.Tag;
import android.support.constraint.ConstraintLayout;
import android.util.Log;

import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.CategoryList;
import com.stampsure.stampsure.beans.UserProfile;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LocalDatabaseHandler extends SQLiteOpenHelper {
    private static final String TAG = "LocalDatabaseHandler";
    private static final String DATABASE_NAME = "STAMPSURE_DATABASE";
    private static final String DATABASE_TABLE = "LOGIN_SCREEN_PROFILE";
    private static final int DATABASE_VERSION = 8;

    private  String ID = "ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String PROFILE_ID = "PROFILE_ID";
    private static final String EMAIl_ID = "EMAIl_ID";
    private static final String PROFILE_IMAGE = "PROFILE_IMAGE";    // For the testing purpose only will be removed at the end.

    private static final String CATEGORY_TABLE = "CATEGORY_TABLE";
    private static final String CATEGORY_ID = "CATEGORY_ID";
    private static final String CATEGORY_NAME = "CATEGORY_NAME";
    private static final String CATEGORY_STATUS = "CATEGORY_STATUS";

    private static final String ASSET_TABLE = "ASSET_TABLE";
    private static final String ASSET_ID = "ASSET_ID";
    private static final String ASSET_UID = "UID";
    private static final String ASSET_NAME = "ASSET_NAME";
    private static final String ASSET_NUMBER = "ASSET_NUMBER";
    private static final String ASSET_CATEGORY_ID = "ASSET_CATEGORY_ID";
    private static final String ASSET_DESC = "ASSET_DESC";
    private static final String ASSET_PRICE = "ASSET_PRICE";
    private static final String ASSET_IMAGE_1 = "ASSET_IMAGE_1";
    private static final String ASSET_IMAGE_2 = "ASSET_IMAGE_2";
    private static final String ASSET_IMAGE_3 = "ASSET_IMAGE_3";
    private static final String ASSET_INVOICE = "ASSET_INVOICE";
    private static final String ASSET_LOCATION = "ASSET_LOCATION";
    private static final String ASSET_AGE = "ASSET_AGE";
    private static final String ASSET_PURCHASE_DATE = "ASSET_PURCHASE_DATE";
    private static final String ASSETS_INSURED_VALUE = "ASSETS_INSURED_VALUE";

    private static final String ASSET_TABLE_SERVER = "ASSET_TABLE_SERVER";
    private static final String ASSET_ID_SERVER = "ASSET_ID_SERVER";
    private static final String ASSET_ID_FROM_SERVER = "ASSET_ID_FROM_SERVER";
    private static final String ASSET_UID_SERVER = "ASSET_UID_SERVER";
    private static final String ASSET_NAME_SERVER = "ASSET_NAME_SERVER";
    private static final String ASSET_NUMBER_SERVER = "ASSET_NUMBER_SERVER";
    private static final String ASSET_CATEGORY_ID_SERVER = "ASSET_CATEGORY_ID_SERVER";
    private static final String ASSET_DESC_SERVER = "ASSET_DESC_SERVER";
    private static final String ASSET_PRICE_SERVER = "ASSET_PRICE_SERVER";
    private static final String ASSET_IMAGE_SERVER_1 = "ASSET_IMAGE_SERVER_1";
    private static final String ASSET_IMAGE_SERVER_2 = "ASSET_IMAGE_SERVER_2";
    private static final String ASSET_IMAGE_SERVER_3 = "ASSET_IMAGE_SERVER_3";
    private static final String ASSET_INVOICE_SERVER = "ASSET_INVOICE_SERVER";
    private static final String ASSET_LOCATION_SERVER = "ASSET_LOCATION_SERVER";
    private static final String ASSET_AGE_SERVER = "ASSET_AGE_SERVER";
    private static final String ASSET_PURCHASE_DATE_SERVER = "ASSET_PURCHASE_DATE_SERVER";
    private static final String ASSETS_INSURED_VALUE_SERVER = "ASSETS_INSURED_VALUE_SERVER";

    public LocalDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE = "CREATE TABLE "+ DATABASE_TABLE + " ( " + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ FIRST_NAME +" TEXT, "
                + PROFILE_ID + " INT, "+ EMAIl_ID + " TEXT, "+ PROFILE_IMAGE +" BLOB)";
        db.execSQL(CREATE_TABLE);

        String CREATE_CATEGORY_TABLE = "CREATE TABLE "+ CATEGORY_TABLE + " ( " + CATEGORY_ID + " INT, "+
                CATEGORY_NAME + " TEXT, "+ CATEGORY_STATUS + " TEXT)";
        db.execSQL(CREATE_CATEGORY_TABLE);

        String CREATE_ASSET_TABLE = "CREATE TABLE " + ASSET_TABLE + " ( " + ASSET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ ASSET_UID + " INT, " + ASSET_NAME + " TEXT, " + ASSET_NUMBER + " INT,"
                + ASSET_CATEGORY_ID + " INT, " + ASSET_DESC + " TEXT, " + ASSET_PRICE + " Double, " + ASSET_IMAGE_1 + " TEXT, " + ASSET_IMAGE_2 + " TEXT, "
                + ASSET_IMAGE_3 + " TEXT, " + ASSET_INVOICE + " TEXT, " + ASSET_LOCATION + " TEXT, " + ASSET_AGE + " INT, " + ASSET_PURCHASE_DATE + " TEXT, "+ ASSETS_INSURED_VALUE +" Double )" ;
        db.execSQL(CREATE_ASSET_TABLE);


        String CREATE_ASSET_TABLE_SERVER = "CREATE TABLE " + ASSET_TABLE_SERVER + " ( " + ASSET_ID_SERVER + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ ASSET_UID_SERVER + " INT, " + ASSET_ID_FROM_SERVER + " INT, "
                + ASSET_NAME_SERVER + " TEXT, " + ASSET_NUMBER_SERVER + " INT,"
                + ASSET_CATEGORY_ID_SERVER + " INT, " + ASSET_DESC_SERVER + " TEXT, " + ASSET_PRICE_SERVER + " Double, " + ASSET_IMAGE_SERVER_1 + " BLOB, " + ASSET_IMAGE_SERVER_2 + " BLOB, "
                + ASSET_IMAGE_SERVER_3 + " BLOB, " + ASSET_INVOICE_SERVER + " BLOB, " + ASSET_LOCATION_SERVER + " TEXT, " + ASSET_AGE_SERVER + " INT, " + ASSET_PURCHASE_DATE_SERVER + " TEXT, " +ASSETS_INSURED_VALUE_SERVER+ " Double )" ;
        db.execSQL(CREATE_ASSET_TABLE_SERVER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE );
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE );
        db.execSQL("DROP TABLE IF EXISTS " + ASSET_TABLE );
        db.execSQL("DROP TABLE IF EXISTS " + ASSET_TABLE_SERVER );
        onCreate(db);
    }

    public void insertProfileForLoginScreen(String first_name, int profile_id, String email, byte[] profile_image){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(FIRST_NAME,first_name);
        contentValues.put(PROFILE_ID,profile_id);
        contentValues.put(EMAIl_ID,email);
        contentValues.put(PROFILE_IMAGE,profile_image);

        long insert_record = db.insert(DATABASE_TABLE,null,contentValues);
        db.close();
     //   return insert_record == -1 ? false : true;
    }

    public UserProfile getProfile(int profile_id){
        SQLiteDatabase db = this.getReadableDatabase();
        UserProfile profile = new UserProfile();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ DATABASE_TABLE + " WHERE PROFILE_ID =?",new String[]{String.valueOf(profile_id)});
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            profile.setfName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
            profile.setId(cursor.getInt(cursor.getColumnIndex(PROFILE_ID)));
            profile.setEmailId(cursor.getString(cursor.getColumnIndex(EMAIl_ID)));
            profile.setProfilePicture(cursor.getBlob(cursor.getColumnIndex(PROFILE_IMAGE)));
        }

        assert cursor != null;
        cursor.close();
        return profile;
    }

    public void deleteExistingProfiles(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE,null,null);
        db.close();
    }


    public boolean putCategory(ArrayList<CategoryList> list){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        long insert_record = 0;
        for (int i = 0; i < list.size(); i ++){
            contentValues.put(CATEGORY_ID,list.get(i).getCategoryId());
            contentValues.put(CATEGORY_NAME,list.get(i).getCategoryName());
            contentValues.put(CATEGORY_STATUS,list.get(i).getCategoryStatus());

            insert_record = db.insert(CATEGORY_TABLE,null,contentValues);
        }
        db.close();
        return insert_record == -1 ? false : true;
    }

    public int getCategoryID(String categoryName){
        SQLiteDatabase db = this.getReadableDatabase();
        int categoryID = 0;
        Cursor cursor = db.rawQuery("SELECT * FROM "+ CATEGORY_TABLE + " WHERE CATEGORY_NAME =?", new String[]{categoryName});
        if(cursor != null &&  cursor.getCount() > 0 ){
            cursor.moveToFirst();
            categoryID = cursor.getInt(cursor.getColumnIndex(CATEGORY_ID));
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return  categoryID;
    }



    public String getCategoryName(int categoryID){
        SQLiteDatabase db = this.getReadableDatabase();
        String categoryName = "";
        Cursor cursor = db.rawQuery("SELECT * FROM "+ CATEGORY_TABLE + " WHERE CATEGORY_ID =?",new String[]{String.valueOf(categoryID)});
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            categoryName = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return categoryName;
    }

    public ArrayList<String> getCategories(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> category = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ CATEGORY_TABLE ,null);
        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                category.add(cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        if(!category.isEmpty()){
            return category;
        }
        return null;
    }
    // Get the records into Stamp Cart.

    public List<Asset> getAssetData(int UID){

        List<Asset> assetList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ ASSET_TABLE + " WHERE UID =?", new String[]{String.valueOf(UID)});

        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                Asset asset = new Asset();
                asset.setId(cursor.getInt(cursor.getColumnIndex(ASSET_ID)));
                asset.setUserId(cursor.getInt(cursor.getColumnIndex(ASSET_UID)));
                asset.setAssetName(cursor.getString(cursor.getColumnIndex(ASSET_NAME)));
                asset.setQuantity(cursor.getInt(cursor.getColumnIndex(ASSET_NUMBER)));
                asset.setCategoryId(cursor.getInt(cursor.getColumnIndex(ASSET_CATEGORY_ID)));
                asset.setBrandDesc(cursor.getString(cursor.getColumnIndex(ASSET_DESC)));
                asset.setBrandPrice(cursor.getDouble(cursor.getColumnIndex(ASSET_PRICE)));

                asset.setImage1Path(cursor.getString(cursor.getColumnIndex(ASSET_IMAGE_1)));
                asset.setImage2Path(cursor.getString(cursor.getColumnIndex(ASSET_IMAGE_2)));
                asset.setImage3Path(cursor.getString(cursor.getColumnIndex(ASSET_IMAGE_3)));
                asset.setInvoicePath(cursor.getString(cursor.getColumnIndex(ASSET_INVOICE)));

                asset.setAssetLocation(cursor.getString(cursor.getColumnIndex(ASSET_LOCATION)));
                asset.setAgeOfAsset(cursor.getInt(cursor.getColumnIndex(ASSET_AGE)));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                try {
                    Date d = sdf.parse(cursor.getString(cursor.getColumnIndex(ASSET_PURCHASE_DATE)));
                    asset.setPurchaseDate(d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                asset.setInsuredValue(cursor.getDouble(cursor.getColumnIndex(ASSETS_INSURED_VALUE)));
                Log.d(TAG,"Item id "+cursor.getInt(cursor.getColumnIndex(ASSET_ID)) + " UID "+ cursor.getInt(cursor.getColumnIndex(ASSET_UID)));
                assetList.add(asset);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return assetList;
    }

    // Get the records into Stamp Cart

    public boolean setAssetData(Asset asset){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ASSET_UID,asset.getUserId());
        contentValues.put(ASSET_NAME,asset.getAssetName());
        contentValues.put(ASSET_NUMBER,asset.getQuantity());
        contentValues.put(ASSET_CATEGORY_ID,asset.getCategoryId());
        contentValues.put(ASSET_DESC,asset.getBrandDesc());
        contentValues.put(ASSET_PRICE,asset.getBrandPrice());
        contentValues.put(ASSET_IMAGE_1,asset.getImage1Path());
        contentValues.put(ASSET_IMAGE_2,asset.getImage2Path());
        contentValues.put(ASSET_IMAGE_3,asset.getImage3Path());
        contentValues.put(ASSET_INVOICE,asset.getInvoicePath());

        contentValues.put(ASSET_LOCATION,asset.getAssetLocation());
        contentValues.put(ASSET_AGE,asset.getAgeOfAsset());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String purchaseDate = df.format(asset.getPurchaseDate());
        contentValues.put(ASSET_PURCHASE_DATE,purchaseDate);
        contentValues.put(ASSETS_INSURED_VALUE,asset.getInsuredValue());
        long insert_record = db.insert(ASSET_TABLE,null,contentValues);

        db.close();
        return insert_record == -1 ? false : true;
    }

    public boolean updateAssetData(int id, Asset asset){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();


        contentValues.put(ASSET_UID,asset.getUserId());
        contentValues.put(ASSET_NAME,asset.getAssetName());
        contentValues.put(ASSET_NUMBER,asset.getQuantity());
        contentValues.put(ASSET_CATEGORY_ID,asset.getCategoryId());
        contentValues.put(ASSET_DESC,asset.getBrandDesc());
        contentValues.put(ASSET_PRICE,asset.getBrandPrice());
        contentValues.put(ASSET_IMAGE_1,asset.getImage1Path());
        contentValues.put(ASSET_IMAGE_2,asset.getImage2Path());
        contentValues.put(ASSET_IMAGE_3,asset.getImage3Path());
        contentValues.put(ASSET_INVOICE,asset.getInvoicePath());

        contentValues.put(ASSET_LOCATION,asset.getAssetLocation());
        contentValues.put(ASSET_AGE,asset.getAgeOfAsset());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String purchaseDate = df.format(asset.getPurchaseDate());
        contentValues.put(ASSET_PURCHASE_DATE,purchaseDate);
        contentValues.put(ASSETS_INSURED_VALUE,asset.getInsuredValue());
        // long insert_record = db.insert(ASSET_TABLE,null,contentValues);
        long update_record = db.update(ASSET_TABLE,contentValues,ASSET_ID+"=?",new String[]{String.valueOf(id)});
        db.close();
        return update_record == -1 ? false : true;
    }

    public void deleteAssetData(int id, int UID){
        SQLiteDatabase db = this.getWritableDatabase();
  //      id++;
        if(id != 0){
            db.delete(ASSET_TABLE,"ASSET_ID=? and UID=?",new String[]{String.valueOf(id),String.valueOf(UID)});
        }else{
            db.delete(ASSET_TABLE,"UID=?",new String[]{String.valueOf(UID)});
        }
        db.close();
    }

    public void setAssetDataFromServer(Asset asset){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ASSET_UID_SERVER,asset.getUserId());
        contentValues.put(ASSET_ID_FROM_SERVER,asset.getId());
        contentValues.put(ASSET_NAME_SERVER,asset.getAssetName());
        contentValues.put(ASSET_NUMBER_SERVER,asset.getQuantity());
        contentValues.put(ASSET_CATEGORY_ID_SERVER,asset.getCategoryId());
        contentValues.put(ASSET_DESC_SERVER,asset.getBrandDesc());
        contentValues.put(ASSET_PRICE_SERVER,asset.getBrandPrice());

        contentValues.put(ASSET_IMAGE_SERVER_1,asset.getImage1());
        contentValues.put(ASSET_IMAGE_SERVER_2,asset.getImage2());
        contentValues.put(ASSET_IMAGE_SERVER_3,asset.getImage3());
        contentValues.put(ASSET_INVOICE_SERVER,asset.getInvoiceImage());

        contentValues.put(ASSET_LOCATION_SERVER,asset.getAssetLocation());
        contentValues.put(ASSET_AGE_SERVER,asset.getAgeOfAsset());
        contentValues.put(ASSETS_INSURED_VALUE_SERVER,asset.getInsuredValue());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String purchaseDate = df.format(asset.getPurchaseDate());
        contentValues.put(ASSET_PURCHASE_DATE_SERVER,purchaseDate);

        long insert_record = db.insert(ASSET_TABLE_SERVER,null,contentValues);
        db.close();
     //   return insert_record == -1 ? false : true;
    }

    public ArrayList<Asset> getAssetDataFromServer(int UID, int assetId, int categoryID, String location){

        ArrayList<Asset> assetList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        if(categoryID == 0 && location.isEmpty() && assetId == 0 ){
            cursor = db.rawQuery("SELECT * FROM "+ ASSET_TABLE_SERVER + " WHERE ASSET_UID_SERVER =?", new String[]{String.valueOf(UID)});
        }
        if(categoryID != 0 && location.isEmpty() && assetId == 0){
            cursor = db.rawQuery("SELECT * FROM "+ ASSET_TABLE_SERVER + " WHERE ASSET_UID_SERVER =? AND ASSET_CATEGORY_ID_SERVER =?", new String[]{String.valueOf(UID),String.valueOf(categoryID)});
        }
        if(categoryID == 0 && !location.isEmpty() && assetId == 0){
            cursor = db.rawQuery("SELECT * FROM "+ ASSET_TABLE_SERVER + " WHERE ASSET_UID_SERVER =? AND ASSET_LOCATION_SERVER =?", new String[]{String.valueOf(UID),location});
        }
        if(categoryID == 0 && location.isEmpty() && assetId != 0){
            cursor = db.rawQuery("SELECT * FROM "+ ASSET_TABLE_SERVER + " WHERE ASSET_UID_SERVER =? AND ASSET_ID_FROM_SERVER =?", new String[]{String.valueOf(UID),String.valueOf(assetId)});
        }

        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                Asset asset = new Asset();
              //  asset.setId(cursor.getInt(cursor.getColumnIndex(ASSET_ID_SERVER)));
                asset.setId(cursor.getInt(cursor.getColumnIndex(ASSET_ID_FROM_SERVER)));
                asset.setUserId(cursor.getInt(cursor.getColumnIndex(ASSET_UID_SERVER)));
                asset.setAssetName(cursor.getString(cursor.getColumnIndex(ASSET_NAME_SERVER)));
                asset.setQuantity(cursor.getInt(cursor.getColumnIndex(ASSET_NUMBER_SERVER)));
                asset.setCategoryId(cursor.getInt(cursor.getColumnIndex(ASSET_CATEGORY_ID_SERVER)));
                asset.setBrandDesc(cursor.getString(cursor.getColumnIndex(ASSET_DESC_SERVER)));
                asset.setBrandPrice(cursor.getDouble(cursor.getColumnIndex(ASSET_PRICE_SERVER)));

                asset.setImage1(cursor.getBlob(cursor.getColumnIndex(ASSET_IMAGE_SERVER_1)));
                asset.setImage2(cursor.getBlob(cursor.getColumnIndex(ASSET_IMAGE_SERVER_2)));
                asset.setImage3(cursor.getBlob(cursor.getColumnIndex(ASSET_IMAGE_SERVER_3)));
                asset.setInvoiceImage(cursor.getBlob(cursor.getColumnIndex(ASSET_INVOICE_SERVER)));

                asset.setAssetLocation(cursor.getString(cursor.getColumnIndex(ASSET_LOCATION_SERVER)));
                asset.setAgeOfAsset(cursor.getInt(cursor.getColumnIndex(ASSET_AGE_SERVER)));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
                try {
                    Date d = sdf.parse(cursor.getString(cursor.getColumnIndex(ASSET_PURCHASE_DATE_SERVER)));
                    asset.setPurchaseDate(d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                asset.setInsuredValue(cursor.getDouble(cursor.getColumnIndex(ASSETS_INSURED_VALUE_SERVER)));
                Log.d(TAG,"Item id "+cursor.getInt(cursor.getColumnIndex(ASSET_ID_SERVER)) + " UID "+ cursor.getInt(cursor.getColumnIndex(ASSET_UID_SERVER)));
                assetList.add(asset);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return assetList;
    }

    public boolean updateAssetDataFromServer(Asset asset){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ASSET_UID_SERVER,asset.getUserId());
        contentValues.put(ASSET_NAME_SERVER,asset.getAssetName());
        contentValues.put(ASSET_NUMBER_SERVER,asset.getQuantity());
        contentValues.put(ASSET_CATEGORY_ID_SERVER,asset.getCategoryId());
        contentValues.put(ASSET_DESC_SERVER,asset.getBrandDesc());
        contentValues.put(ASSET_PRICE_SERVER,asset.getBrandPrice());
        contentValues.put(ASSET_IMAGE_SERVER_1,asset.getImage1());
        contentValues.put(ASSET_IMAGE_SERVER_2,asset.getImage2());
        contentValues.put(ASSET_IMAGE_SERVER_3,asset.getImage3());
        contentValues.put(ASSET_INVOICE_SERVER,asset.getInvoiceImage());

        contentValues.put(ASSET_LOCATION_SERVER,asset.getAssetLocation());
        contentValues.put(ASSET_AGE_SERVER,asset.getAgeOfAsset());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String purchaseDate = df.format(asset.getPurchaseDate());
        contentValues.put(ASSET_PURCHASE_DATE_SERVER,purchaseDate);
        contentValues.put(ASSETS_INSURED_VALUE_SERVER,asset.getInsuredValue());
        long update_record = db.update(ASSET_TABLE_SERVER,contentValues,ASSET_ID_FROM_SERVER+"=?",new String[]{String.valueOf(asset.getId())});
        db.close();
        return update_record == -1 ? false : true;
    }

    public void deleteAssetServerData(int id, int UID){
        SQLiteDatabase db = this.getWritableDatabase();
        //      id++;
        if(id != 0){
            db.delete(ASSET_TABLE_SERVER,"ASSET_ID_FROM_SERVER=? and ASSET_UID_SERVER=?",new String[]{String.valueOf(id),String.valueOf(UID)});
        }else{
        //    db.delete(ASSET_TABLE_SERVER,"ASSET_UID_SERVER=?",new String[]{String.valueOf(UID)});
            db.delete(ASSET_TABLE_SERVER,null,null);
        }
        db.close();
    }

    public void deleteCategory(){
        Log.d(TAG, "deleteCategory: record");
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CATEGORY_TABLE,null,null);
        db.close();
    }

}
