package com.stampsure.stampsure.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;


import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.StampActivity;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.AssetStampCart;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.ItemListAdapter;
import com.stampsure.stampsure.beans.URLComponent;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.support.v7.app.ActionBar;

import id.zelory.compressor.Compressor;

public class ItemListFragment extends Fragment {

    private static String TAG = "ItemListFragment";

    List<Asset> assetList;
    RecyclerView recyclerView;

    private List<AssetStampCart> assetStampCartList;
    public Context context;
    private EditText input_insured_value;
    private OnFragmentInteractionListener mListener;
    private int UID;
    private ArrayList<Asset> assetsList;
    public ItemListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        Log.d(TAG,"onCreate Started");

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle("Stamp Cart");

        Button id_button_capture =  view.findViewById(R.id.id_button_capture);
        Button id_button_stamp_out = view.findViewById(R.id.id_button_stamp_out);
        TextView id_total_value = view.findViewById(R.id.id_total_value);
        Switch toggleSwitch = view.findViewById(R.id.id_switch);
        input_insured_value = view.findViewById(R.id.input_insured_value);

        initImageBitmap(view);

        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        context = this.getActivity().getWindow().getContext();

        input_insured_value.setEnabled(false);
        input_insured_value.setBackgroundResource(0);

        toggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked){
                    input_insured_value.setEnabled(true);
                    input_insured_value.setBackgroundResource(R.drawable.editext_border);
                }else{
                    input_insured_value.setEnabled(false);
                    input_insured_value.setBackgroundResource(0);
                    input_insured_value.setText("");
                }
            }
        });

        id_button_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getActivity().getApplicationContext(),"Size "+ assetStampCartList.size(),Toast.LENGTH_SHORT ).show();
                CaptureFragment captureFragment = new CaptureFragment();
                fragmentTransaction.replace(R.id.id_main_frame, captureFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        id_button_stamp_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(assetStampCartList.get(0).getAsset().getInsuredValue() == 0) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                    View mView = getLayoutInflater().inflate(R.layout.insured_value_dialog,null);
                    Switch id_insured_switch = (Switch) mView.findViewById(R.id.id_insured_switch);
                    final EditText id_insured_value = (EditText) mView.findViewById(R.id.id_insured_value);
                    Button id_insured_ok = (Button) mView.findViewById(R.id.id_insured_ok);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    id_insured_value.setEnabled(false);
                    id_insured_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(isChecked){
                                id_insured_value.setEnabled(true);
                            }else{
                                id_insured_value.setEnabled(false);
                            }
                        }
                    });
                    id_insured_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(dialog.isShowing()){
                                dialog.cancel();
                            }
                            List<Asset> stampList = new ArrayList<>();
                            for (int i = 0; i < assetStampCartList.size(); i++) {
                                Double insured_value = 0.00;
                                if(id_insured_value.getText().toString().isEmpty()){
                                    insured_value = 0.00;
                                }else{
                                    insured_value = Double.parseDouble(id_insured_value.getText().toString());
                                }
                                assetStampCartList.get(i).getAsset().setInsuredValue(insured_value);
                                stampList.add(assetStampCartList.get(i).getAsset());
                                stampList.get(i).setId(0);
                            }
                            new HttpStampCart().execute(stampList);  // Send the list of all items to package
                        }
                    });
                  /*  mBuilder.setView(mView);
                    AlertDialog dialog = mBuilder.create();
                    dialog.show();*/
                }else {
                    List<Asset> stampList = new ArrayList<>();
                    for (int i = 0; i < assetStampCartList.size(); i++) {
                        stampList.add(assetStampCartList.get(i).getAsset());
                        stampList.get(i).setId(0);
                    }
                    new HttpStampCart().execute(stampList);  // Send the list of all items to package
                }
            }
        });

        Double total_value = 0.00;
        for (int i = 0; i < assetStampCartList.size(); i++){
            total_value = total_value + assetStampCartList.get(i).getAsset().getBrandPrice();
        }
        String total_amount = "R"+" "+String.valueOf(total_value);
        id_total_value.setText(total_amount);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @SuppressLint("SetTextI18n")
    public void initImageBitmap(View view){

        UID = ComponentBuilder.getUIDFromActivity(getActivity().getApplicationContext());
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getActivity().getApplicationContext());
        assetList = databaseHandler.getAssetData(UID);

        assetStampCartList = new ArrayList<>();

        for(int i = 0; i < assetList.size(); i ++){
            AssetStampCart assetStampCart = new AssetStampCart();
            Asset asset = new Asset();
            asset.setId(assetList.get(i).getId());
            asset.setImage1Path(assetList.get(i).getImage1Path());
            asset.setImage2Path(assetList.get(i).getImage2Path());
            asset.setImage3Path(assetList.get(i).getImage3Path());
            asset.setInvoicePath(assetList.get(i).getInvoicePath());


            asset.setAssetName(assetList.get(i).getAssetName());
            asset.setBrandDesc(assetList.get(i).getBrandDesc());
            asset.setBrandPrice(assetList.get(i).getBrandPrice());
            asset.setCategoryId(assetList.get(i).getCategoryId());
            asset.setUserId(assetList.get(i).getUserId());

            asset.setImage1(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),assetList.get(i).getImage1Path()));
            asset.setImage2(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),assetList.get(i).getImage2Path()));
            asset.setImage3(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),assetList.get(i).getImage3Path()));
            asset.setInvoiceImage(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),assetList.get(i).getInvoicePath()));

            asset.setStatus("Y");

            asset.setQuantity(Integer.parseInt(assetList.get(i).getQuantity().toString()));
            asset.setAssetLocation(assetList.get(i).getAssetLocation());

            asset.setAgeOfAsset(assetList.get(i).getAgeOfAsset());

            asset.setUserId(UID);
            asset.setCategoryId(assetList.get(i).getCategoryId());
            asset.setPurchaseDate(assetList.get(i).getPurchaseDate());
            asset.setInsuredValue(assetList.get(i).getInsuredValue());

            if(assetList.get(i).getInsuredValue() == null){
                input_insured_value.setText("");
            }else {
                input_insured_value.setText(assetList.get(i).getInsuredValue().toString());
            }
            assetStampCart.isSelected();
            assetStampCart.setAsset(asset);


            assetStampCartList.add(assetStampCart);

        }
        initRecyclerView(view,assetStampCartList);
    }
    @TargetApi(23)
    private void initRecyclerView(View view , List<AssetStampCart> list){
        recyclerView = (RecyclerView) view.findViewById(R.id.id_recycler_view);
        ItemListAdapter recyclerViewAdapter = new ItemListAdapter(UID,list,getContext());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewAdapter.notifyDataSetChanged();
    }

    public class HttpStampCart extends AsyncTask<List<Asset>, Void, Boolean>{
        private ProgressDialog progressDialog;
        @SafeVarargs
        @Override
        protected final Boolean doInBackground(List<Asset>... lists) {
            Boolean response = false;
            try{
                String stampURL = URLComponent.addAssetURL;
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                response = restTemplate.postForObject(stampURL, lists[0], Boolean.class);
            }catch(Exception e){
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPreExecute() {
            //    Toast.makeText(getContext(),"Fetching the assets", Toast.LENGTH_SHORT).show();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Uploading items");
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);
            if(response){

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(context);
                databaseHandler.deleteAssetData(0,  UID);

                final AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(context);
                createOptionDialog.setTitle("Success! Asset Stamped");
                createOptionDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new HttpGetAssetList().execute();
                    }
                });
                createOptionDialog.show();

            }else{
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(context, "Item Stamping out failure", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class HttpGetAssetList extends AsyncTask<Void,Void,ArrayList<Asset>> {
        ArrayList<Asset> assetList = new ArrayList<>();
        private ProgressDialog progressDialog;
        @Override
        protected ArrayList<Asset> doInBackground(Void... voids) {
            UID = ComponentBuilder.getUIDFromActivity(getContext());
            String stampURL = URLComponent.getAssetURL + "/" + UID;
            try{
                Asset[] forNow = ComponentBuilder.getRestTemplate().getForObject(stampURL, Asset[].class);
                assetList = new ArrayList(Arrays.asList(forNow));
            }catch(Exception e){
                e.printStackTrace();
            }
            return assetList;
        }
        @Override
        protected void onPreExecute() {
        //    Toast.makeText(getContext(),"Fetching the assets", Toast.LENGTH_SHORT).show();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Updating Dashboard");
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(ArrayList<Asset> assets) {
            int asset_numbers = 0;
            if(assets.size() != 0) {
                UID = ComponentBuilder.getUIDFromActivity(getContext());
                LocalDatabaseHandler databaseHandler1 = new LocalDatabaseHandler(getContext());

                databaseHandler1.deleteAssetServerData(0, UID);

                for (int i = 0; i < assets.size(); i++) {
                    LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
                    databaseHandler.setAssetDataFromServer(assets.get(i));
                    asset_numbers = i;
                    Log.d(TAG, "onPostExecute: Storing " + asset_numbers + " " + assets.get(i).getAssetName());
                }

                if (assets.size() == (asset_numbers + 1)) {
                    FragmentManager pie_fragmentManager = getFragmentManager();
                    FragmentTransaction pie_fragmentTransaction = pie_fragmentManager.beginTransaction();
                    DashboardManager dashboardFragment = new DashboardManager();
                    pie_fragmentTransaction.replace(R.id.id_main_frame, dashboardFragment);
//
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    pie_fragmentTransaction.addToBackStack(null);
                    pie_fragmentTransaction.commit();
                }
            }else{
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getContext(),"No items stored, please capture",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
