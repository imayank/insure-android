package com.stampsure.stampsure.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.AssetStampCart;
import com.stampsure.stampsure.beans.CategorizedValue;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.URLComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DashboardManager extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "DashboardManager";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SectionPagerAdapter mSectionPagerAdapter;
    private ViewPager pager;

    private int UID;
    private ArrayList<Asset> assetsList;
    private OnFragmentInteractionListener mListener;

    public DashboardManager() {
        // Required empty public constructor
    }

    public static DashboardManager newInstance(String param1, String param2) {
        DashboardManager fragment = new DashboardManager();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dashboard_manager, container, false);
        BottomNavigationView navigation = ((Activity)getContext()).getWindow().getDecorView().findViewById(R.id.bottom_navigation_bar);
        navigation.getMenu().getItem(0).setChecked(true);

        ComponentBuilder.setLocation(getContext(),"");
        ComponentBuilder.setCategoryID(getContext(),0);

        mSectionPagerAdapter = new SectionPagerAdapter( getChildFragmentManager());

        pager = (ViewPager) view.findViewById(R.id.id_pager);
        TabLayout tabLayout = (TabLayout)view.findViewById(R.id.tabDots);

        tabLayout.setupWithViewPager(pager, true);
        pager.setAdapter(mSectionPagerAdapter);
        mSectionPagerAdapter.notifyDataSetChanged();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SectionPagerAdapter extends FragmentPagerAdapter {
        private SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch(position){
                case 0:
                 return DashBoardCategory.newInstance("1","2");
                case 1:
                    return DashboardLocation.newInstance("1","2");
                case 2:
                    return DashboardAge.newInstance("1","2");
                default:
                    return null;
            }
        }
        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
