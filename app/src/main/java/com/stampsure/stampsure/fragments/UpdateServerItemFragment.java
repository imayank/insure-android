package com.stampsure.stampsure.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.RotateImage;
import com.stampsure.stampsure.beans.URLComponent;

import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UpdateServerItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UpdateServerItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpdateServerItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CircleImageView id_item_server_pic1;
    private CircleImageView id_item_server_pic2;
    private CircleImageView id_item_server_pic3;
    private CircleImageView id_item_server_invoice;

    private TextView id_updateImg_server_1;
    private TextView id_updateImg_server_2;
    private TextView id_updateImg_server_3;
    private TextView id_update_server_Invoice;

    private TextView id_add_server_Image;
    private TextView id_add_server_Invoice;

    private int imageUpdate1 = 0;
    private int imageUpdate2 = 0;
    private int imageUpdate3 = 0;
    private int invoiceUpdate = 0;

    private EditText id_items_server_number;
    private EditText id_asset_server_name;
    private EditText id_asset_server_desc;
    private EditText id_asset_server_value;
    private EditText id_server_location;
    private EditText id_asset_purchase_server_date;
    private Button id_update_server_item;
    private Spinner id_asset_server_category;

    private String categorySelected = "";
    private ArrayList<Asset> assetList;
    private Asset asset;
    private int UID;
    private Context context;

    private RelativeLayout id_relative_server_1;
    private RelativeLayout id_relative_server_2;
    private RelativeLayout id_relative_server_3;
    private RelativeLayout id_relative_server_4;
    private int assetId;

    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    private String mImageFileLocation = "";
    private String imageFileLocation1 = "";
    private String imageFileLocation2 = "";
    private String imageFileLocation3 = "";
    private String imageFileLocation4 = "";

    private String imageLocation1 = "";
    private String imageLocation2 = "";
    private String imageLocation3 = "";
    private String invoiceLocation = "";

    public String imageFileName = "";
    private int resultCanceled = 0;

    private int prefCategory = 0;
    private String prefLocation = "";


    private OnFragmentInteractionListener mListener;

    public UpdateServerItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpdateServerItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UpdateServerItemFragment newInstance(String param1, String param2) {
        UpdateServerItemFragment fragment = new UpdateServerItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_server_item, container, false);

        id_item_server_pic1 = (CircleImageView) view.findViewById(R.id.id_item_server_pic1);
        id_item_server_pic2 = (CircleImageView) view.findViewById(R.id.id_item_server_pic2);
        id_item_server_pic3 = (CircleImageView) view.findViewById(R.id.id_item_server_pic3);
        id_item_server_invoice = (CircleImageView) view.findViewById(R.id.id_item_server_invoice);

        id_updateImg_server_1 = (TextView)view.findViewById(R.id.id_updateImg_server_1);
        id_updateImg_server_2 = (TextView)view.findViewById(R.id.id_updateImg_server_2);
        id_updateImg_server_3 = (TextView)view.findViewById(R.id.id_updateImg_server_3);
        id_update_server_Invoice = (TextView)view.findViewById(R.id.id_update_server_Invoice);

        id_items_server_number = (EditText)view.findViewById(R.id.id_items_server_number);
        id_asset_server_name = (EditText)view.findViewById(R.id.id_asset_server_name);
        id_asset_server_desc = (EditText)view.findViewById(R.id.id_asset_server_desc);
        id_asset_server_value = (EditText)view.findViewById(R.id.id_asset_server_value);
        id_server_location = (EditText)view.findViewById(R.id.id_server_location);
        id_asset_purchase_server_date = (EditText)view.findViewById(R.id.id_asset_purchase_server_date);
        id_update_server_item = (Button)view.findViewById(R.id.id_update_server_item);
        id_asset_server_category = (Spinner)view.findViewById(R.id.id_asset_server_category);

        id_relative_server_1 = (RelativeLayout) view.findViewById(R.id.id_relative_server_1);
        id_relative_server_2 = (RelativeLayout) view.findViewById(R.id.id_relative_server_2);
        id_relative_server_3 = (RelativeLayout) view.findViewById(R.id.id_relative_server_3);
        id_relative_server_4 = (RelativeLayout) view.findViewById(R.id.id_relative_server_4);

        id_add_server_Image = (TextView) view.findViewById(R.id.id_add_server_Image);
        id_add_server_Invoice = (TextView) view.findViewById(R.id.id_add_server_Invoice);

        id_relative_server_2.setVisibility(View.GONE);
        id_relative_server_3.setVisibility(View.GONE);
        id_relative_server_4.setVisibility(View.GONE);

        id_add_server_Image.setVisibility(View.GONE);
        id_add_server_Invoice.setVisibility(View.GONE);

       // asset = (Asset)getArguments().getSerializable("AssetId");
        assetId = (int)getArguments().getSerializable("AssetId");
        UID = ComponentBuilder.getUIDFromActivity(getContext());
        final LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
        assetList = databaseHandler.getAssetDataFromServer(UID,assetId,0,"");
        asset = assetList.get(0);

        context = getActivity().getApplicationContext();

        if(asset.getImage1() != null) {

            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage1())
                    .into(id_item_server_pic1);
        }else{
            id_add_server_Image.setVisibility(View.VISIBLE);
        }

        if(asset.getImage2() != null) {

            id_relative_server_2.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage2())
                    .into(id_item_server_pic2);
        }else{
            id_add_server_Image.setVisibility(View.VISIBLE);
        }

        if(asset.getImage3() != null) {

            id_relative_server_3.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage3())
                    .into(id_item_server_pic3);
        }else{
            id_add_server_Image.setVisibility(View.VISIBLE);
        }

        if(asset.getInvoiceImage() != null) {

            id_relative_server_4.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getInvoiceImage())
                    .into(id_item_server_invoice);
        }else{
            id_add_server_Invoice.setVisibility(View.VISIBLE);
        }

        id_items_server_number.setText(asset.getQuantity().toString());
        id_asset_server_name.setText(asset.getAssetName());
        id_asset_server_desc.setText(asset.getBrandDesc());
        id_asset_server_value.setText(asset.getBrandPrice().toString());
        id_server_location.setText(asset.getAssetLocation());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String storeDate = df.format(asset.getPurchaseDate());
        id_asset_purchase_server_date.setText(storeDate);

        // Set<String> setId = ComponentBuilder.getCategoryFromActivity(context);

        LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(context);

        ArrayList<String> category = dbScreenUserHandler.getCategories();
        category.add(0,"select category");

        ArrayAdapter<String> categoryList = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, category);
        categoryList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_asset_server_category.setAdapter(categoryList);
        id_asset_server_category.setSelection(asset.getCategoryId());
        id_asset_server_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Do a call to database to get the categories
                categorySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                id_asset_purchase_server_date.setText(sdf.format(myCalendar.getTime()));
            }

        };

        id_asset_purchase_server_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity().getApplicationContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        id_update_server_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Asset updateAsset = new Asset();
/*

                if (imageUpdate1 == 0){
                    imageLocation1 = asset.getImage1Path();
                }else{
                    imageLocation1 = imageFileLocation1;
                }

                if (imageUpdate2 == 0){
                    imageLocation2 = asset.getImage2Path();
                }else{
                    imageLocation2 = imageFileLocation2;
                }

                if (imageUpdate3 == 0){
                    imageLocation3 = asset.getImage3Path();
                }else{
                    imageLocation3 = imageFileLocation3;
                }

                if (invoiceUpdate == 0){
                    invoiceLocation = asset.getInvoicePath();
                }else{
                    invoiceLocation = imageFileLocation4;
                }
*/

                updateAsset.setId(asset.getId());
                updateAsset.setStatus("Y");

                if(!imageFileLocation1.isEmpty()){
                    updateAsset.setImage1(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),imageLocation1));
                }else{
                    updateAsset.setImage1(asset.getImage1());
                }

                if( !imageLocation2.isEmpty()){
                    updateAsset.setImage2(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),imageLocation2));
                }else{
                    updateAsset.setImage2(asset.getImage2());
                }

                if(!imageLocation3.isEmpty()){
                    updateAsset.setImage3(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),imageLocation3));
                }else{
                    updateAsset.setImage3(asset.getImage3());
                }

                if(!imageFileLocation4.isEmpty()){
                    updateAsset.setInvoiceImage(ComponentBuilder.getCompressedFile(getActivity().getApplicationContext(),imageFileLocation4));
                }else{
                    updateAsset.setInvoiceImage(asset.getInvoiceImage());
                }


                updateAsset.setQuantity(Integer.parseInt(id_items_server_number.getText().toString()));
                updateAsset.setAssetName(id_asset_server_name.getText().toString());
                updateAsset.setBrandDesc(id_asset_server_desc.getText().toString());

                updateAsset.setBrandPrice(Double.parseDouble(id_asset_server_value.getText().toString()));
                updateAsset.setAssetLocation(id_server_location.getText().toString());

                int UID = ComponentBuilder.getUIDFromActivity(context);
                updateAsset.setUserId(UID);

                LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(context);
                dbScreenUserHandler.getCategoryID(categorySelected);
                updateAsset.setCategoryId(dbScreenUserHandler.getCategoryID(categorySelected));

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date entityDate = (Date) dateFormat.parse(id_asset_purchase_server_date.getText().toString());
                    updateAsset.setPurchaseDate(entityDate);
                    updateAsset.setAgeOfAsset(ComponentBuilder.getAssetAge(entityDate));

                    String currentDate = dateFormat.format(new Date());
                    updateAsset.setLastUpdated(dateFormat.parse(currentDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                boolean updated = databaseHandler.updateAssetDataFromServer(updateAsset);

                if(updated){
                    Bundle bundle = new Bundle();
                    prefCategory = ComponentBuilder.getCategoryID(getContext());
                    prefLocation = ComponentBuilder.getLocation(getContext());

                    if(prefCategory != 0){
                        bundle.putSerializable("UID_category", prefCategory);
                    }else{
                        bundle.putSerializable("UID_location", prefCategory);
                    }

                    new HTTPUpdateItem().execute(updateAsset);

                    UidDashboard uidDashboard = new UidDashboard();

                    uidDashboard.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.id_main_frame, uidDashboard);

                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }else{
                    Toast.makeText(getContext(),"Local update failed",Toast.LENGTH_SHORT).show();
                }
            }
        });

        id_updateImg_server_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_updateImg_server_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_updateImg_server_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_update_server_Invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_add_server_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(asset.getImage1() == null){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }else if(asset.getImage2() == null){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }else if(asset.getImage3() == null){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            }
        });
        id_add_server_Invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class HTTPUpdateItem extends AsyncTask<Asset,Void,Boolean>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
/*            if (aBoolean == false){
                Toast.makeText(getContext(),"Item Update failed on server",Toast.LENGTH_SHORT).show();
            }else{

            }*/
        }

        @Override
        protected Boolean doInBackground(Asset... assets) {
            String stampURL = URLComponent.updateAsset;
            Boolean response = false;
            try{
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                restTemplate.put(stampURL, assets[0], Boolean.class);
            }catch(Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }

    public void takePicture(int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
        startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private File createPictureFile(String CategoryName, int itemId, int pictureId) throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (itemId == 1) {
            imageFileName = "Temp" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }else if(itemId == 2){
            imageFileName = "Invoice" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }
        File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),CategoryName);

        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        File image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);
        if(!(resultCode == 0)) {
            resultCanceled = 0;
            if (requestCode == 1880) {
                imageUpdate1 = 1;
                imageFileLocation1 = mImageFileLocation;
                imageLocation1 = imageFileLocation1;
                if(id_relative_server_1.getVisibility() == View.GONE){
                    id_relative_server_1.setVisibility(View.VISIBLE);
                }

                id_item_server_pic1.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
            }

            if (requestCode == 1881) {
                imageUpdate2 = 1;
                imageFileLocation2 = mImageFileLocation;
                imageLocation2 = imageFileLocation2;
                if(id_relative_server_2.getVisibility() == View.GONE){
                    id_relative_server_2.setVisibility(View.VISIBLE);
                }
                id_item_server_pic2.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));

            }
            if (requestCode == 1882) {
                imageUpdate3 = 1;
                imageFileLocation3 = mImageFileLocation;
                imageLocation3 = imageFileLocation3;
                if(id_relative_server_3.getVisibility() == View.GONE){
                    id_relative_server_3.setVisibility(View.VISIBLE);
                }
                id_item_server_pic3.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                id_add_server_Image.setVisibility(View.GONE);

            }
            if (requestCode == 1883) {
                invoiceUpdate = 1;
                imageFileLocation4 = mImageFileLocation;
                if(id_relative_server_4.getVisibility() == View.GONE){
                    id_relative_server_4.setVisibility(View.VISIBLE);
                }
                id_item_server_invoice.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                id_add_server_Invoice.setVisibility(View.GONE);
            }
        }else{
            resultCanceled = 1;
        }
        if (resultCanceled == 1){
            switch (requestCode){
                case 1880:
                    imageUpdate1 = 0;
                    break;
                case 1881:
                    imageUpdate2 = 0;
                    break;
                case 1882:
                    imageUpdate3 = 0;
                case 1883:
                    invoiceUpdate = 0;
                    break;
                default:
                    break;
            }
        }
    }
}
