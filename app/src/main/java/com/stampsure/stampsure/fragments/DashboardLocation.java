package com.stampsure.stampsure.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.CategorizedValue;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.PieAmountFormatter;
import com.stampsure.stampsure.beans.PieColors;
import android.support.v7.app.ActionBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashBoardCategory.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashBoardCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardLocation extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String DEFAULT_CURRENCY = "R";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private static String TAG = "DashBoardLocation";
    private float[] yData;
    private String[] xData;
    PieChart pieChart;
    private int UID;
    public TextView id_location;
    public TextView id_location_item_total;
    public Button id_details;
    public ArrayList<Asset> UidAssetList;
    public String location;
    private LinearLayout location_detail_layout;

    public ArrayList<Asset> input_assetList;

    public DashboardLocation() {
        // Required empty public constructor
    }

    public static DashboardLocation newInstance(String param1, String param2) {
        DashboardLocation fragment = new DashboardLocation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
         //   input_assetList = (ArrayList<Asset>) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard_location, container, false);
        Log.d(TAG, "onCreateView: starting to plot the chart");

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Dashboard");

        pieChart = (PieChart) view.findViewById(R.id.id_pie_chart);
        Description description = new Description();
        description.setText("Item value per Location");
     //   description.setTextColor(ColorTemplate.JOYFUL_COLORS[2]);
        description.setTextColor(ColorTemplate.JOYFUL_COLORS[4]);
        pieChart.setDescription(description);
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Item Location");
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);

        UID = ComponentBuilder.getUIDFromActivity(getActivity().getApplicationContext());
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
        input_assetList =  databaseHandler.getAssetDataFromServer(UID,0,0,"");

        onExecute(input_assetList);

        id_location = (TextView) view.findViewById(R.id.id_location);
        id_location_item_total = (TextView) view.findViewById(R.id.id_location_item_total);
        id_details = (Button) view.findViewById(R.id.id_details);
        location_detail_layout = (LinearLayout)view.findViewById(R.id.location_detail_layout);
        location_detail_layout.setVisibility(View.GONE);
        id_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location.isEmpty()) {
                    Toast.makeText(getContext(), "Category not selected", Toast.LENGTH_SHORT).show();
                } else if (UidAssetList == null) {
                    Toast.makeText(getContext(), "Items list blank", Toast.LENGTH_SHORT).show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("UID_location", location);

                    ComponentBuilder.setLocation(getContext(),location);

                    UidDashboard uidDashboard = new UidDashboard();
                    uidDashboard.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.id_main_frame, uidDashboard);

                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //   assetList.cancel(true);
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    protected void onExecute(ArrayList<Asset> assets) {
        Double[] unCategorizeValue = new Double[assets.size()];
        String[] unCategorizeLocation = new String[assets.size()];

        for (int i = 0; i < assets.size(); i++) {
            unCategorizeValue[i] = assets.get(i).getBrandPrice();
            unCategorizeLocation[i] = assets.get(i).getAssetLocation();
        }

        CategorizedValue categorizedValue = dataCategorization(unCategorizeLocation, unCategorizeValue);
        Double[] valueArray = categorizedValue.getTotalValue();
        String[] locationArray = categorizedValue.getLocation();
        yData = new float[valueArray.length];
        xData = new String[locationArray.length];
        for (int i = 0; i < valueArray.length; i++) {
            yData[i] = (float) (valueArray[i] * 1.00);
            xData[i] = locationArray[i];
        }
        addDataset();

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                location_detail_layout.setVisibility(View.VISIBLE);
                Log.d(TAG, "onValueSelected: Value selected from Chart");
                Log.d(TAG, "onValueSelected: e " + e.toString() + " h " + h.toString());

                PieEntry pe = (PieEntry) e;
                location = pe.getLabel();

                for (int i = 0; i < xData.length; i++) {
                    if (xData[i].equals(location)) {
                        id_location.setText(xData[i]);
                        String currencyValue = DEFAULT_CURRENCY + " "+ String.valueOf(yData[i]);
                        id_location_item_total.setText(currencyValue);
                        UidAssetList = getAssetList(location);
                    }
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    public ArrayList<Asset> getAssetList(String location) {
        ArrayList<Asset> UidAssetListExtracted = new ArrayList<>();
        for (int i = 0; i < input_assetList.size(); i++) {
            if (location.equals(input_assetList.get(i).getAssetLocation())) {
                UidAssetListExtracted.add(input_assetList.get(i));
            }
        }
        return UidAssetListExtracted;
    }


    private void addDataset() {
        Log.d(TAG, "addDataset: adding data in pie chart");
        ArrayList<PieEntry> yEntries = new ArrayList<>();

        for (int i = 0; i < yData.length; i++) {
            yEntries.add(new PieEntry(yData[i], xData[i]));
        }

        PieDataSet pieDataSet = new PieDataSet(yEntries, "");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueFormatter(new PieAmountFormatter());

        pieDataSet.setColors(PieColors.getPieColors());

        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.setDrawSliceText(false);

        pieChart.invalidate();
    }

    @TargetApi(Build.VERSION_CODES.N)
    public CategorizedValue dataCategorization(String[] unCategorizeLocation, Double[] unCategorizeValue) {

        Set<String> unique = new HashSet<String>(Arrays.asList(unCategorizeLocation));
        Double[] totalValue = new Double[unique.size()];
        String[] uniqueLocation = new String[unique.size()];

        int j = 0;
        for (Iterator<String> setValue = unique.iterator(); setValue.hasNext(); j++) {
            totalValue[j] = 0.00;
            uniqueLocation[j] = "";
            String it = setValue.next();
            for (int i = 0; i < unCategorizeLocation.length; i++) {
                if (unCategorizeLocation[i].equals(it)) {
                    totalValue[j] = totalValue[j] + unCategorizeValue[i];
                    uniqueLocation[j] = it;
                }
            }
        }
        CategorizedValue categorizedValue = new CategorizedValue();
        categorizedValue.setTotalValue(totalValue);
        categorizedValue.setLocation(uniqueLocation);

        return categorizedValue;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


}



