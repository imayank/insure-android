package com.stampsure.stampsure.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.stampsure.stampsure.AutoComplete.CustomAdapter;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.RotateImage;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import android.support.v7.app.ActionBar;

public class AddItemFragment extends Fragment {
    private static final String TAG = "AddItemFragment";
    private OnFragmentInteractionListener mListener;

    ImageView id_item_pic;
    EditText id_asset_age;
    Spinner id_location;
    Spinner id_asset_category;

    private EditText id_items_number;
    private EditText id_asset_name;
  //  private EditText id_asset_desc;
    private AutoCompleteTextView id_asset_desc;
    private EditText id_asset_value;
    private EditText id_asset_purchase_date;
    private Button id_update_item;
    private Context contextDialog = null;
    private String categorySelected = "";
    private String id_location_selection = "";
    private ArrayList<String> imagePathList;

    // Need this list to be populated on the fly
    private static final String[] item_desc = new String[]{"Samsung j1","Samsung j2","Samsung j3","Samsung j4","Samsung j5","Samsung j6",
            "Samsung TV","Couch","Curton","Bed","Chair","Head Phone","Speaker Phone"};

    public AddItemFragment() {
        // Required empty public constructor
    }

    @TargetApi(23)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle("Save Item");

        contextDialog = this.getActivity().getWindow().getContext();

        id_item_pic = view.findViewById(R.id.id_item_pic);
        id_items_number =  view.findViewById(R.id.id_items_number);
        id_asset_name =  view.findViewById(R.id.id_asset_name);
        id_asset_desc =  view.findViewById(R.id.id_asset_desc);
        id_asset_value =  view.findViewById(R.id.id_asset_value);
        id_location =  view.findViewById(R.id.id_location);
        id_asset_purchase_date =  view.findViewById(R.id.id_asset_purchase_date);
        id_asset_age =  view.findViewById(R.id.id_asset_age);
        id_update_item =  view.findViewById(R.id.id_update_item);
        id_asset_category =  view.findViewById(R.id.id_asset_category);

        id_items_number.setText("1");

        imagePathList = (ArrayList<String>) getArguments().getSerializable("ImagePathList");
        assert imagePathList != null;
        String input_image_path = imagePathList.get(0);
        // RotateImage.decodeSampledBitmapFromResource(getContext(),getResources(), input_image_path, 97, 90)
        id_item_pic.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), input_image_path, 97, 90));

    //    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_list_item_1,item_desc);
        CustomAdapter<String> adapter = new CustomAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_list_item_1,item_desc);
        id_asset_desc.setAdapter(adapter);


        //Populate the number of items in
        Set<String> setId = ComponentBuilder.getCategoryFromActivity(getContext());

        ArrayList<String> category = new ArrayList<>(setId);
        category.add(0,"select category");

        ArrayAdapter<String> categoryList = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, category);
        categoryList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_asset_category.setAdapter(categoryList);
        id_asset_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Do a call to database to get the categories
                categorySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.locations, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_location.setAdapter(spinnerAdapter);

        id_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_location_selection = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                id_asset_purchase_date.setText(sdf.format(myCalendar.getTime()));
            }
        };

        id_asset_purchase_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        id_update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(id_update_item.getWindowToken(), 0);

                if(id_items_number.getText().toString().isEmpty()) {
                    id_items_number.setError("Please select at least one quantity");
                    id_items_number.requestFocus();
                }

                if(TextUtils.isEmpty(id_asset_name.getText().toString())){
                    id_asset_name.setError("Please give item name");
                    id_asset_name.requestFocus();
                }


                if(TextUtils.isEmpty(id_asset_value.getText().toString()) || Double.parseDouble(id_asset_value.getText().toString()) == 0.00){
                    id_asset_value.setError("Please put amount for item");
                    id_asset_value.requestFocus();
                }

                if(categorySelected.equals("select category")){
                    Toast.makeText(getContext(), "Please select category", Toast.LENGTH_SHORT).show();
                    categorySelected = "";
                }

                if(TextUtils.isEmpty(id_asset_purchase_date.getText().toString()) ){
                    id_asset_purchase_date.setError("Please select Date");
                    id_asset_purchase_date.requestFocus();
                }

                if(!categorySelected.isEmpty() &&
                        !id_asset_name.getText().toString().isEmpty() &&
                        !id_asset_value.getText().toString().isEmpty() &&
                        !id_location_selection.isEmpty() &&
                        !id_asset_purchase_date.getText().toString().isEmpty() ) {

                    Asset asset = new Asset();

                    asset.setImage1(null);
                    asset.setImage2(null);
                    asset.setImage3(null);
                    asset.setInvoiceImage(null);


                    asset.setQuantity(Integer.parseInt(id_items_number.getText().toString()));
                    asset.setAssetName(id_asset_name.getText().toString());
                    asset.setBrandDesc(id_asset_desc.getText().toString());

                    asset.setBrandPrice(Double.parseDouble(id_asset_value.getText().toString()));
                    asset.setAssetLocation(id_location_selection);



                    int UID = ComponentBuilder.getUIDFromActivity(getContext());
                    asset.setUserId(UID);

                    LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(getContext());

                    asset.setCategoryId(dbScreenUserHandler.getCategoryID(categorySelected));

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date entityDate = null;
                    try {
                        entityDate = (Date) dateFormat.parse(id_asset_purchase_date.getText().toString());
                        asset.setPurchaseDate(entityDate);
                        asset.setAgeOfAsset(ComponentBuilder.getAssetAge(entityDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    assert entityDate != null;
                    asset.setAgeOfAsset(ComponentBuilder.getAssetAge(entityDate));

                    asset.setImage1Path(imagePathList.get(0));
                    asset.setImage2Path(imagePathList.get(1));
                    asset.setImage3Path(imagePathList.get(2));
                    asset.setInvoicePath(imagePathList.get(3));

                    LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
                    boolean insertAsset = databaseHandler.setAssetData(asset);

                    if (insertAsset) {
                        final FragmentManager fragmentManager = getFragmentManager();
                        assert fragmentManager != null;
                        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(contextDialog);
                        createOptionDialog.setTitle("Item saved, Add more");
                        createOptionDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // This is to show that the item has been stred on the local machine and will be used as a StampCart.
                                CaptureFragment captureFragment = new CaptureFragment();
                                fragmentTransaction.replace(R.id.id_main_frame, captureFragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }
                        });
                        createOptionDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // This is to show that the item has been stored on the local machine and will be used as a StampCart.
                                ItemListFragment itemListFragment = new ItemListFragment();
                                fragmentTransaction.replace(R.id.id_main_frame, itemListFragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }
                        });
                        createOptionDialog.show();
                    } else {
                        Toast.makeText(getContext(), "Item stored locally", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getContext(), "Incomplete information", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
