package com.stampsure.stampsure.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.AssetStampCart;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.ItemServerListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UidDashboard.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UidDashboard#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UidDashboard extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView id_recycler_categoryView;

    private TextView id_text_label_categoryView;
    private TextView id_total_value_categoryView;
    private TextView id_button_capture_categoryView;
    private ArrayList<Asset> assets;
    private List<AssetStampCart> assetStampCartList;
    private int UID;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    public UidDashboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UidDashboard.
     */
    // TODO: Rename and change types and number of parameters
    public static UidDashboard newInstance(String param1, String param2) {
        UidDashboard fragment = new UidDashboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_uid_dashboard, container, false);
        UID = ComponentBuilder.getUIDFromActivity(getActivity().getApplicationContext());
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());

        if( getArguments() != null && getArguments().containsKey("UID_location")){
            String location = (String)getArguments().getSerializable("UID_location");
            assets =  databaseHandler.getAssetDataFromServer(UID,0,0,location);
        }else if(getArguments() != null && getArguments().containsKey("UID_category")){
            int categoryId = (int)getArguments().getSerializable("UID_category");
            assets =  databaseHandler.getAssetDataFromServer(UID,0,categoryId,"");
        }

         if(assets != null){
            assetStampCartList = new ArrayList<>();
            for(int i = 0; i < assets.size(); i++){
                AssetStampCart assetStampCart = new AssetStampCart();
                assetStampCart.isSelected();
                assetStampCart.setAsset(assets.get(i));
                assetStampCartList.add(assetStampCart);
            }
            initRecyclerView(view,assetStampCartList);
        }else{
            Toast.makeText(getActivity().getApplicationContext(),"Error while fetching assets from server.",Toast.LENGTH_SHORT).show();
        }
        id_text_label_categoryView = (TextView) view.findViewById(R.id.id_text_label_categoryView);

        id_button_capture_categoryView = (Button) view.findViewById(R.id.id_button_capture_categoryView);

        id_button_capture_categoryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentManager fragmentManager = getFragmentManager();
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                CaptureFragment captureFragment = new CaptureFragment();
                fragmentTransaction.replace(R.id.id_main_frame, captureFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        Double total_value = 0.00;
        for (int i = 0; i < assetStampCartList.size(); i++){
            total_value = total_value + assetStampCartList.get(i).getAsset().getBrandPrice();
        }

        id_total_value_categoryView = (TextView) view.findViewById(R.id.id_total_value_categoryView);
        id_total_value_categoryView.setText(String.valueOf(total_value));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @TargetApi(23)
    private void initRecyclerView(View view , List<AssetStampCart> list){
        id_recycler_categoryView = (RecyclerView) view.findViewById(R.id.id_recycler_categoryView);

        ItemServerListAdapter recyclerViewAdapter = new ItemServerListAdapter(UID,list,getContext());
        id_recycler_categoryView.setAdapter(recyclerViewAdapter);
        id_recycler_categoryView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewAdapter.notifyDataSetChanged();

    }
}
