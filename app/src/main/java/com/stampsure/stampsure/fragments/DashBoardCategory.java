package com.stampsure.stampsure.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.CategorizedValue;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.PieAmountFormatter;
import com.stampsure.stampsure.beans.PieColors;
import com.stampsure.stampsure.beans.URLComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import id.zelory.compressor.Compressor;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashBoardCategory.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashBoardCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashBoardCategory extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String DEFAULT_CURRENCY = "R";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private static String TAG = "DashBoardCategory";
    private float[] yData;
    private Integer[] xData;
    private PieChart pieChart;
    private int UID;
    public TextView id_category;
    public TextView id_category_total;
    public Button id_details;
    public ArrayList<Asset> UidAssetList;
    public ArrayList<Asset> input_assetList;
    public int categoryID;
    private LinearLayout category_detail_layout;

    public DashBoardCategory() {
        // Required empty public constructor
    }

    public static DashBoardCategory newInstance(String param1, String param2) {
        DashBoardCategory fragment = new DashBoardCategory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
          //  input_assetList = (ArrayList<Asset>) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board_category, container, false);
        Log.d(TAG, "onCreateView: starting to plot the chart");

        android.support.v7.app.ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Dashboard");

        pieChart = (PieChart) view.findViewById(R.id.id_pie_chart);
        Description description = new Description();
        description.setText("Item value per Category");
        description.setTextColor(ColorTemplate.JOYFUL_COLORS[4]);
        pieChart.setDescription(description);
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Item Category");
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);

        UID = ComponentBuilder.getUIDFromActivity(getActivity().getApplicationContext());
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
        input_assetList =  databaseHandler.getAssetDataFromServer(UID,0,0,"");
        onExecute(input_assetList);

        category_detail_layout = (LinearLayout) view.findViewById(R.id.category_detail_layout);
        id_category = (TextView) view.findViewById(R.id.id_category);
        id_category_total = (TextView) view.findViewById(R.id.id_category_total);
        id_details = (Button) view.findViewById(R.id.id_details);
        category_detail_layout.setVisibility(View.GONE);

        id_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryID == 0) {
                    Toast.makeText(getContext(), "category not selected", Toast.LENGTH_SHORT).show();
                } else if (UidAssetList == null) {
                    Toast.makeText(getContext(), "Items list blank", Toast.LENGTH_SHORT).show();
                } else {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("UID_category", categoryID);
                    ComponentBuilder.setCategoryID(getContext(),categoryID);

                    UidDashboard uidDashboard = new UidDashboard();

                    uidDashboard.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.id_main_frame, uidDashboard);

                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    protected void onExecute(ArrayList<Asset> assets) {
        Double[] unCategorizeValue = new Double[assets.size()];
        Integer[] unCategorizeCategory = new Integer[assets.size()];
        for (int i = 0; i < assets.size(); i++) {
            unCategorizeValue[i] = assets.get(i).getBrandPrice();
            unCategorizeCategory[i] = assets.get(i).getCategoryId();
        }

        CategorizedValue categorizedValue = dataCategorization(unCategorizeCategory, unCategorizeValue);
        Double[] valueArray = categorizedValue.getTotalValue();
        Integer[] categoryArray = categorizedValue.getCategoryId();
        yData = new float[valueArray.length];
        xData = new Integer[categoryArray.length];
        for (int i = 0; i < valueArray.length; i++) {

            yData[i] = (float) (valueArray[i] * 1.00);
            xData[i] = categoryArray[i];
        }
        addPieDataset();

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                category_detail_layout.setVisibility(View.VISIBLE);
                PieEntry pe = (PieEntry) e;
                categoryID = Integer.parseInt(pe.getLabel());
                for (int i = 0; i < xData.length; i++) {
                    if (xData[i] == categoryID) {
                        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());
                        String categoryName = databaseHandler.getCategoryName(Integer.parseInt(pe.getLabel()));
                        id_category.setText(categoryName);
                        String currencyValue = DEFAULT_CURRENCY+ " "+ String.valueOf(yData[i]);
                        id_category_total.setText(currencyValue);
                        UidAssetList = getAssetList(categoryID);
                    }
                }
            }

            @Override
            public void onNothingSelected() {
            }
        });
    }

    public ArrayList<Asset> getAssetList(int categoryId) {
        ArrayList<Asset> UidAssetListExtracted = new ArrayList<>();
        for (int i = 0; i < input_assetList.size(); i++) {
            if (categoryId == input_assetList.get(i).getCategoryId()) {
                UidAssetListExtracted.add(input_assetList.get(i));
            }
        }
        return UidAssetListExtracted;
    }

    private void addPieDataset() {
        Log.d(TAG, "addDataset: adding data in pie chart");
        ArrayList<PieEntry> yEntries = new ArrayList<>();

        for (int i = 0; i < yData.length; i++) {
       //     yEntries.add(new PieEntry(yData[i], xData[i].toString()));
            yEntries.add(new PieEntry(yData[i], xData[i].toString()));
        }

        PieDataSet pieDataSet = new PieDataSet(yEntries, "");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueFormatter(new PieAmountFormatter());
        pieDataSet.setColors(PieColors.getPieColors());

        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.setDrawSliceText(false);

        pieChart.invalidate();

    }

    @TargetApi(Build.VERSION_CODES.N)
    public CategorizedValue dataCategorization(Integer[] unCategorizeCategory, Double[] unCategorizeValue) {

        Set<Integer> unique = new HashSet<Integer>(Arrays.asList(unCategorizeCategory));
        Double[] totalValue = new Double[unique.size()];
        Integer[] uniqueCategory = new Integer[unique.size()];

        int j = 0;
        for (Iterator<Integer> setValue = unique.iterator(); setValue.hasNext(); j++) {
            totalValue[j] = 0.00;
            uniqueCategory[j] = 0;
            Integer it = setValue.next();
            for (int i = 0; i < unCategorizeCategory.length; i++) {
                if (it.equals(unCategorizeCategory[i])) {
                    totalValue[j] = totalValue[j] + unCategorizeValue[i];
                    uniqueCategory[j] = it;
                }
            }
        }
        CategorizedValue categorizedValue = new CategorizedValue();
        categorizedValue.setTotalValue(totalValue);
        categorizedValue.setCategoryId(uniqueCategory);

        return categorizedValue;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}



