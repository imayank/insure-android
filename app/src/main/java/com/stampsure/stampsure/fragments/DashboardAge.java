package com.stampsure.stampsure.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.app.ActionBar;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.PieAmountFormatter;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardAge.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardAge#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardAge extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private BarChart id_bar_graph;
    private int UID;
    public ArrayList<Asset> input_assetList;
    public ArrayList<Asset> sorted_assetList;
    private OnFragmentInteractionListener mListener;

    public DashboardAge() {

    }
    public static DashboardAge newInstance(String param1, String param2) {
        DashboardAge fragment = new DashboardAge();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard_age, container, false);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Dashboard");

        UID = ComponentBuilder.getUIDFromActivity(getActivity().getApplicationContext());
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(getContext());

        input_assetList =  databaseHandler.getAssetDataFromServer(UID,0,0,"");
        id_bar_graph = (BarChart)view.findViewById(R.id.id_bar_graph);
      //  onExecute(input_assetList);
        onTryExecute(input_assetList);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    protected void onExecute(final ArrayList<Asset> assets) {
   //     sorted_assetList = new ArrayList<>();
        Collections.sort(assets, new Comparator<Asset>(){
            @Override
            public int compare(Asset o1, Asset o2) {
                if(o1.getAgeOfAsset().equals(o2.getAgeOfAsset()))
                    return 0;
                return o1.getAgeOfAsset() > o2.getAgeOfAsset() ? -1 : 1;
            }
        });

        ArrayList<Integer> ageDate = new ArrayList<>();
        ArrayList<Asset> selectedAsset = new ArrayList<>();
        for(int i = 0; i < 5; i ++){
            ageDate.add(assets.get(i).getAgeOfAsset());
            selectedAsset.add(assets.get(i));
        }

        Set set = new HashSet<>(ageDate);

        ArrayList<BarEntry> barEntries1 = new ArrayList<>();
        ArrayList<BarEntry> barEntries2 = new ArrayList<>();
        ArrayList<BarEntry> barEntries3 = new ArrayList<>();
        ArrayList<BarEntry> barEntries4 = new ArrayList<>();
        ArrayList<BarEntry> barEntries5 = new ArrayList<>();

        int i = 0;
        for (Iterator<Integer> setValue = set.iterator(); setValue.hasNext(); i++) {
            Integer age = setValue.next();
            for(int j = 0; j < selectedAsset.size(); j++){
                if(i == 0 && age.equals(assets.get(j).getAgeOfAsset())){
                    barEntries1.add(new BarEntry((float)(assets.get(i).getAgeOfAsset()),(float)(assets.get(j).getBrandPrice() * 1.00 )));
                }
                if(i == 1 && age.equals(assets.get(j).getAgeOfAsset())){
                    barEntries2.add(new BarEntry((float)(assets.get(i).getAgeOfAsset()),(float)(assets.get(j).getBrandPrice() * 1.00 )));
                }
                if(i == 2 && age.equals(assets.get(j).getAgeOfAsset())){
                    barEntries3.add(new BarEntry((float)(assets.get(i).getAgeOfAsset()),(float)(assets.get(j).getBrandPrice() * 1.00 )));
                }
                if(i == 3 && age.equals(assets.get(j).getAgeOfAsset())){
                    barEntries4.add(new BarEntry((float)(assets.get(i).getAgeOfAsset()),(float)(assets.get(j).getBrandPrice() * 1.00 )));
                }
                if(i == 4 && age.equals(assets.get(j).getAgeOfAsset())){
                    barEntries5.add(new BarEntry((float)(assets.get(i).getAgeOfAsset()),(float)(assets.get(j).getBrandPrice() * 1.00 )));
                }
            }
        }

        BarDataSet barDataSet1 = new BarDataSet(barEntries1,"Value1"); barDataSet1.setColor(Color.BLACK); barDataSet1.setBarBorderWidth(1f);
        BarDataSet barDataSet2 = new BarDataSet(barEntries2,"Value2"); barDataSet2.setColor(Color.BLUE);  barDataSet2.setBarBorderWidth(1f);
        BarDataSet barDataSet3 = new BarDataSet(barEntries3,"Value3"); barDataSet3.setColor(Color.CYAN);  barDataSet3.setBarBorderWidth(1f);
        BarDataSet barDataSet4 = new BarDataSet(barEntries4,"Value4"); barDataSet4.setColor(Color.GRAY);  barDataSet4.setBarBorderWidth(1f);
        BarDataSet barDataSet5 = new BarDataSet(barEntries5,"Value5"); barDataSet5.setColor(Color.GREEN); barDataSet5.setBarBorderWidth(1f);

        // barDataSet.setValueFormatter(new PieAmountFormatter());

        float groupSpace = 0.06f;
        float barSpace = 0.02f; // x2 dataset
        float barWidth = 0.15f; // x2 dataset

        BarData barData = new BarData(barDataSet1,barDataSet2,barDataSet3,barDataSet4,barDataSet5);
        barData.setValueFormatter(new PieAmountFormatter());
        barData.setBarWidth(barWidth);
        id_bar_graph.setData(barData);

       // id_bar_graph.setTouchEnabled(true);
      //  id_bar_graph.setDragEnabled(true);
      //  id_bar_graph.setScaleEnabled(true);
        id_bar_graph.setDrawGridBackground(false);
        id_bar_graph.setDrawBarShadow(false);
        id_bar_graph.setDrawValueAboveBar(true);
        id_bar_graph.getDescription().setEnabled(false);

        XAxis xAxis = id_bar_graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(5);
        xAxis.setLabelRotationAngle(-45);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new IAxisValueFormatter(){
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                DecimalFormat formatter = new DecimalFormat("#######0");
                return formatter.format((int)value  );

            }

        });

        xAxis.setGranularityEnabled(true);

        xAxis.setAxisMaximum(5);
        xAxis.setAxisMinimum(1);

        YAxis yAxis = id_bar_graph.getAxisRight();
        yAxis.setDrawGridLines(true);

        id_bar_graph.groupBars(1,groupSpace,barSpace);
        id_bar_graph.invalidate();
    }
    protected void onTryExecute(final ArrayList<Asset> assets){
        Collections.sort(assets, new Comparator<Asset>() {
            @Override
            public int compare(Asset o1, Asset o2) {
                if (o1.getAgeOfAsset().equals(o2.getAgeOfAsset()))
                    return 0;
                return o1.getAgeOfAsset() > o2.getAgeOfAsset() ? -1 : 1;
            }
        });
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        final ArrayList<String> age = new ArrayList<>();

        for(int i = 0; i < 5; i ++){

            barEntries.add(new BarEntry(i + 1,(float)(assets.get(i).getBrandPrice() * 1)));
            age.add(assets.get(i).getAgeOfAsset() + " Months \n" + assets.get(i).getAssetName());
        }

        BarDataSet bar1Set = new BarDataSet(barEntries , "Item Value");

        bar1Set.setAxisDependency(YAxis.AxisDependency.LEFT);
        bar1Set.setDrawValues(true);
        bar1Set.setValueFormatter(new PieAmountFormatter());
        final float barWidth = 0.2f;

        BarData barData = new BarData(bar1Set);
        barData.setBarWidth(barWidth);



        id_bar_graph.setData(barData);

        id_bar_graph.getDescription().setEnabled(false);
    //    id_bar_graph.setBackgroundColor(backgroundColor);
        id_bar_graph.setDrawGridBackground(false);
        id_bar_graph.setDrawBarShadow(true);
        id_bar_graph.setDoubleTapToZoomEnabled(false);
        id_bar_graph.setPinchZoom(false);
        id_bar_graph.setScaleEnabled(false);

// show legend on Top Left
        Legend legend = id_bar_graph.getLegend();
        legend.setEnabled(true);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);


      //  leftAxis.setAxisMaximum(200);

        XAxis xAxis = id_bar_graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setAxisMinimum(0);
        xAxis.setGranularity(1f);
        xAxis.setTextSize(12);
        xAxis.setAxisMaximum(barData.getXMax() + 1);
        xAxis.setLabelRotationAngle(-40f);
        xAxis.setTextSize(10);

        IAxisValueFormatter xAxisFormatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value >= 0 && value <= age.size() - 1) {
                    return age.get((int) value);
                }
                return "";
            }
        };
        xAxis.setValueFormatter(xAxisFormatter);
        id_bar_graph.invalidate();
    }
}
