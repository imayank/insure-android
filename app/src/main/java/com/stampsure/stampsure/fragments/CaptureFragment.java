package com.stampsure.stampsure.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.RequestPermissionHandler;
import com.stampsure.stampsure.beans.RotateImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import android.support.v7.app.ActionBar;

public class CaptureFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private static String TEMP_IMAGE = "Temp_Image";

    private LinearLayout id_first_row;
    private LinearLayout id_second_row;
    private RelativeLayout id_id_layout_image_1;
    private RelativeLayout id_id_layout_image_2;
    private RelativeLayout id_id_layout_image_3;
    private RelativeLayout id_id_layout_image_4;

    private ImageView id_item_image_1;
    private ImageView id_item_image_2;
    private ImageView id_item_image_3;
    private ImageView id_item_image_4;

    private ImageView id_refresh_1;
    private ImageView id_refresh_2;
    private ImageView id_refresh_3;
    private ImageView id_refresh_4;

    private ImageView buttonInvoice;
    private ImageView button_add_photo;

    Button id_update_item;

    private String mImageFileLocation = "";
    private String imageFileLocation1 = "";
    private String imageFileLocation2 = "";
    private String imageFileLocation3 = "";
    private String imageFileLocation4 = "";
    public String imageFileName = "";
    private int resultCanceled = 0;

    private ArrayList<String> imagePath = new ArrayList<>();

    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    public CaptureFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle("Capture Item");

        View view =  inflater.inflate(R.layout.capturefragment, container, false);

        id_first_row = view.findViewById(R.id.id_first_row);
        id_second_row = view.findViewById(R.id.id_second_row);
        id_id_layout_image_1 = view.findViewById(R.id.id_id_layout_image_1);
        id_id_layout_image_2 = view.findViewById(R.id.id_id_layout_image_2);
        id_id_layout_image_3 = view.findViewById(R.id.id_id_layout_image_3);
        id_id_layout_image_4 = view.findViewById(R.id.id_id_layout_image_4);

        id_item_image_1 = view.findViewById(R.id.id_item_image_1);
        id_item_image_2 = view.findViewById(R.id.id_item_image_2);
        id_item_image_3 = view.findViewById(R.id.id_item_image_3);
        id_item_image_4 = view.findViewById(R.id.id_item_image_4);

        id_refresh_1    = view.findViewById(R.id.id_refresh_1);
        id_refresh_2    = view.findViewById(R.id.id_refresh_2);
        id_refresh_3    = view.findViewById(R.id.id_refresh_3);
        id_refresh_4    = view.findViewById(R.id.id_refresh_4);

        button_add_photo  = view.findViewById(R.id.button_add_photo);
        id_update_item    = view.findViewById(R.id.id_update_item);
        buttonInvoice     = view.findViewById(R.id.id_buttonInvoice);


        id_second_row.setVisibility(View.GONE);
        id_id_layout_image_2.setVisibility(View.GONE);
        id_id_layout_image_3.setVisibility(View.GONE);
        id_id_layout_image_4.setVisibility(View.GONE);

        buttonInvoice.setEnabled(false);

        button_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = getImageRequestCode(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,1);
            }
        });

        buttonInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,2);
            }
        });

        id_refresh_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,1);
            }
        });
        id_refresh_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,1);
            }
        });
        id_refresh_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,1);
            }
        });
        id_refresh_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                captureImage(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,2);
            }
        });

        id_update_item.setOnClickListener(new View.OnClickListener() {
            @TargetApi(23)
            @Override
            public void onClick(View v) {

                if(imageFileLocation1.isEmpty() && imageFileLocation2.isEmpty() && imageFileLocation3.isEmpty()){
                    Toast.makeText(getContext(),"Take at least 1 picture", Toast.LENGTH_SHORT).show();
                }else {
                    if (!imageFileLocation1.isEmpty()) {
                        imagePath.add(imageFileLocation1);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation2.isEmpty()) {
                        imagePath.add(imageFileLocation2);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation3.isEmpty()) {
                        imagePath.add(imageFileLocation3);
                    }else{
                        imagePath.add("");
                    }
                    if (!imageFileLocation4.isEmpty()) {
                        imagePath.add(imageFileLocation4);
                    }else{
                        imagePath.add("");
                    }

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    Bundle imageList = new Bundle();
                    imageList.putSerializable("ImagePathList", imagePath);
                    AddItemFragment addItemFragment = new AddItemFragment() ;
                    addItemFragment.setArguments(imageList);
                    fragmentTransaction.replace(R.id.id_main_frame, addItemFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });

        return view;
    }
    @TargetApi(23)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
      //  super.onActivityResult(requestCode, resultCode, data);
        if(!(resultCode == 0)) {
            resultCanceled = 0;
            if (requestCode == 1880) {
                imageFileLocation1 = mImageFileLocation;
                id_item_image_1.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_1.setVisibility(View.VISIBLE);
            }

            if (requestCode == 1881) {
                id_id_layout_image_2.setVisibility(View.VISIBLE);
                imageFileLocation2 = mImageFileLocation;
                id_item_image_2.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_2.setVisibility(View.VISIBLE);
            }
            if (requestCode == 1882) {
                id_second_row.setVisibility(View.VISIBLE);
                id_id_layout_image_3.setVisibility(View.VISIBLE);
                imageFileLocation3 = mImageFileLocation;
                id_item_image_3.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                button_add_photo.setEnabled(false);
                buttonInvoice.setEnabled(true);
                id_refresh_3.setVisibility(View.VISIBLE);
            }
            if (requestCode == 1883) {
                id_second_row.setVisibility(View.VISIBLE);
                id_id_layout_image_4.setVisibility(View.VISIBLE);
                imageFileLocation4 = mImageFileLocation;
                id_item_image_4.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                buttonInvoice.setEnabled(true);
                id_refresh_4.setVisibility(View.VISIBLE);
            }
        }else{
            resultCanceled = 1;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private int getImageRequestCode(int request_code){
        int IMAGE_REQUEST_CODE = 0;
        switch (request_code){
            case 0:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1880;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1880:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1881;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1881:
                if (resultCanceled != 1) {
                    IMAGE_REQUEST_CODE = 1882;
                }else{
                    IMAGE_REQUEST_CODE = request_code;
                }
                break;
            case 1882:
                IMAGE_REQUEST_CODE = request_code;
                break;
            default:
                IMAGE_REQUEST_CODE = 1880;
        }
        return IMAGE_REQUEST_CODE;
    }

    private File createPictureFile(String CategoryName, int itemId, int pictureId) throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (itemId == 1) {
            imageFileName = "Temp" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }else if(itemId == 2){
            imageFileName = "Invoice" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }
        File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),CategoryName);

        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        Log.d("CaptureFragment",fileStorageDirectory.toString());
        File image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        return image;
    }

    private void captureImage(int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE,int imageCode){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createPictureFile(TEMP_IMAGE, imageCode, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));

        startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }
}
