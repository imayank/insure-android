package com.stampsure.stampsure.fragments;

import android.app.DatePickerDialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.beans.Asset;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.RotateImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UpdateItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UpdateItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpdateItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CircleImageView id_item_pic1;
    private CircleImageView id_item_pic2;
    private CircleImageView id_item_pic3;
    private CircleImageView id_item_invoice;

    private TextView id_updateImg_1;
    private TextView id_updateImg_2;
    private TextView id_updateImg_3;
    private TextView id_updateInvoice;

    private TextView id_addImage;
    private TextView id_addInvoice;

    private int imageUpdate1 = 0;
    private int imageUpdate2 = 0;
    private int imageUpdate3 = 0;
    private int invoiceUpdate = 0;

    private EditText id_items_number;
    private EditText id_asset_name;
    private EditText id_asset_desc;
    private EditText id_asset_value;
    private EditText id_location;
    private EditText id_asset_purchase_date;
    private Button id_update_item;
    private Spinner id_asset_category;
    private String categorySelected = "";
    private OnFragmentInteractionListener mListener;
    private Asset asset;
    private Context context;

    private RelativeLayout id_relative_1;
    private RelativeLayout id_relative_2;
    private RelativeLayout id_relative_3;
    private RelativeLayout id_relative_4;

    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    private String mImageFileLocation = "";
    private String imageFileLocation1 = "";
    private String imageFileLocation2 = "";
    private String imageFileLocation3 = "";
    private String imageFileLocation4 = "";

    private String imageLocation1 = "";
    private String imageLocation2 = "";
    private String imageLocation3 = "";
    private String invoiceLocation = "";

    public String imageFileName = "";
    private int resultCanceled = 0;

    public UpdateItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpdateItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UpdateItemFragment newInstance(String param1, String param2) {
        UpdateItemFragment fragment = new UpdateItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_update_item, container, false);

        android.support.v7.app.ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Update Cart Item");

        id_item_pic1 = (CircleImageView) view.findViewById(R.id.id_item_pic1);
        id_item_pic2 = (CircleImageView) view.findViewById(R.id.id_item_pic2);
        id_item_pic3 = (CircleImageView) view.findViewById(R.id.id_item_pic3);
        id_item_invoice = (CircleImageView) view.findViewById(R.id.id_item_invoice);

        id_updateImg_1 = (TextView)view.findViewById(R.id.id_updateImg_1);
        id_updateImg_2 = (TextView)view.findViewById(R.id.id_updateImg_2);
        id_updateImg_3 = (TextView)view.findViewById(R.id.id_updateImg_3);
        id_updateInvoice = (TextView)view.findViewById(R.id.id_updateInvoice);

        id_items_number = (EditText)view.findViewById(R.id.id_items_number);
        id_asset_name = (EditText)view.findViewById(R.id.id_asset_name);
        id_asset_desc = (EditText)view.findViewById(R.id.id_asset_desc);
        id_asset_value = (EditText)view.findViewById(R.id.id_asset_value);
        id_location = (EditText)view.findViewById(R.id.id_location);
        id_asset_purchase_date = (EditText)view.findViewById(R.id.id_asset_purchase_date);
        id_update_item = (Button)view.findViewById(R.id.id_update_item);
        id_asset_category = (Spinner)view.findViewById(R.id.id_asset_category);

        id_relative_1 = (RelativeLayout) view.findViewById(R.id.id_relative_1);
        id_relative_2 = (RelativeLayout) view.findViewById(R.id.id_relative_2);
        id_relative_3 = (RelativeLayout) view.findViewById(R.id.id_relative_3);
        id_relative_4 = (RelativeLayout) view.findViewById(R.id.id_relative_4);

        id_addImage = (TextView) view.findViewById(R.id.id_addImage);
        id_addInvoice = (TextView) view.findViewById(R.id.id_addInvoice);

        id_relative_2.setVisibility(View.GONE);
        id_relative_3.setVisibility(View.GONE);
        id_relative_4.setVisibility(View.GONE);

        id_addImage.setVisibility(View.GONE);
        id_addInvoice.setVisibility(View.GONE);


        asset = (Asset)getArguments().getSerializable("Asset");
        context = getActivity().getApplicationContext();

        if(!asset.getImage1Path().isEmpty()) {
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage1Path())
                    .into(id_item_pic1);
        }else{
            id_addImage.setVisibility(View.VISIBLE);
        }
        if(!asset.getImage2Path().isEmpty()) {
            id_relative_2.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage2Path())
                    .into(id_item_pic2);
        }else{
            id_addImage.setVisibility(View.VISIBLE);
        }
        if(!asset.getImage3Path().isEmpty()) {
            id_relative_3.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getImage3Path())
                    .into(id_item_pic3);
        }else{
            id_addImage.setVisibility(View.VISIBLE);
        }
        if(!asset.getInvoicePath().isEmpty()) {
            id_relative_4.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(asset.getInvoicePath())
                    .into(id_item_invoice);
        }else{
            id_addInvoice.setVisibility(View.VISIBLE);
        }

        imageLocation1 = asset.getImage1Path();
        imageLocation2 = asset.getImage2Path();
        imageLocation3 = asset.getImage3Path();
        invoiceLocation = asset.getInvoicePath();

        id_items_number.setText(asset.getQuantity().toString());
        id_asset_name.setText(asset.getAssetName());
        id_asset_desc.setText(asset.getBrandDesc());
        id_asset_value.setText(asset.getBrandPrice().toString());
        id_location.setText(asset.getAssetLocation());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String storeDate = df.format(asset.getPurchaseDate());
        id_asset_purchase_date.setText(storeDate);

      //  Set<String> setId = ComponentBuilder.getCategoryFromActivity(context);
        LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(context);

       // ArrayList<String> category = new ArrayList(setId);
        ArrayList<String> category = dbScreenUserHandler.getCategories();
        category.add(0,"select category");

        ArrayAdapter<String> categoryList = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, category);
        categoryList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_asset_category.setAdapter(categoryList);
        id_asset_category.setSelection(asset.getCategoryId());

        id_asset_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Do a call to database to get the categories
                categorySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                id_asset_purchase_date.setText(sdf.format(myCalendar.getTime()));
            }

        };

        id_addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageLocation1.isEmpty()){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }else if(imageLocation2.isEmpty()){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }else if(imageLocation3.isEmpty()){
                    CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                    takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            }
        });
        id_addInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_asset_purchase_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity().getApplicationContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        id_update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Asset updateAsset = new Asset();

                if (imageUpdate1 == 0){
                    imageLocation1 = asset.getImage1Path();
                }else{
                    imageLocation1 = imageFileLocation1;
                }

                if (imageUpdate2 == 0){
                    imageLocation2 = asset.getImage2Path();
                }else{
                    imageLocation2 = imageFileLocation2;
                }

                if (imageUpdate3 == 0){
                    imageLocation3 = asset.getImage3Path();
                }else{
                    imageLocation3 = imageFileLocation3;
                }

                if (invoiceUpdate == 0){
                    invoiceLocation = asset.getInvoicePath();
                }else{
                    invoiceLocation = imageFileLocation4;
                }

                updateAsset.setQuantity(Integer.parseInt(id_items_number.getText().toString()));
                updateAsset.setAssetName(id_asset_name.getText().toString());
                updateAsset.setBrandDesc(id_asset_desc.getText().toString());

                updateAsset.setBrandPrice(Double.parseDouble(id_asset_value.getText().toString()));
                updateAsset.setAssetLocation(id_location.getText().toString());

                int UID = ComponentBuilder.getUIDFromActivity(context);
                updateAsset.setUserId(UID);

                LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(context);
                updateAsset.setCategoryId(dbScreenUserHandler.getCategoryID(categorySelected));

                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date entityDate = (Date) format.parse(id_asset_purchase_date.getText().toString());
                    updateAsset.setPurchaseDate(entityDate);
                    updateAsset.setAgeOfAsset(ComponentBuilder.getAssetAge(entityDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }



                updateAsset.setImage1Path(imageLocation1);
                updateAsset.setImage2Path(imageLocation2);
                updateAsset.setImage3Path(imageLocation3);
                updateAsset.setInvoicePath(invoiceLocation);

                boolean insertAsset = dbScreenUserHandler.updateAssetData(asset.getId(), updateAsset);
                if(insertAsset){
                    Toast.makeText(context, "Update Success", Toast.LENGTH_SHORT).show();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    ItemListFragment itemListFragment = new ItemListFragment();
                    fragmentTransaction.replace(R.id.id_main_frame, itemListFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }else{
                    Toast.makeText(context, "Update Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });

        id_updateImg_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1880;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_updateImg_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1881;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_updateImg_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1882;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        id_updateInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1883;
                takePicture(CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void takePicture(int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createPictureFile("Temp_Image", 1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
        startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private File createPictureFile(String CategoryName, int itemId, int pictureId) throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (itemId == 1) {
            imageFileName = "Temp" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }else if(itemId == 2){
            imageFileName = "Invoice" + "_" + itemId + "_" + pictureId + "_" + timeStamp + "_";
        }
        File fileStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),CategoryName);

        if(!fileStorageDirectory.exists()){
            fileStorageDirectory.mkdirs();
        }
        Log.d("CaptureFragment",fileStorageDirectory.toString());
        File image = File.createTempFile(imageFileName,".jpg",fileStorageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);
        if(!(resultCode == 0)) {
            resultCanceled = 0;
            if (requestCode == 1880) {
                imageUpdate1 = 1;
                imageFileLocation1 = mImageFileLocation;
                imageLocation1 = imageFileLocation1;
                if(id_relative_1.getVisibility() == View.GONE){
                    id_relative_1.setVisibility(View.VISIBLE);
                }

                id_item_pic1.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
            }

            if (requestCode == 1881) {
                imageUpdate2 = 1;
                imageFileLocation2 = mImageFileLocation;
                imageLocation2 = imageFileLocation2;
                if(id_relative_2.getVisibility() == View.GONE){
                    id_relative_2.setVisibility(View.VISIBLE);
                }
                id_item_pic2.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));

            }
            if (requestCode == 1882) {
                imageUpdate3 = 1;
                imageFileLocation3 = mImageFileLocation;
                imageLocation3 = imageFileLocation3;
                if(id_relative_3.getVisibility() == View.GONE){
                    id_relative_3.setVisibility(View.VISIBLE);
                }
                id_item_pic3.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                id_addImage.setVisibility(View.GONE);

            }
            if (requestCode == 1883) {
                invoiceUpdate = 1;
                imageFileLocation4 = mImageFileLocation;
                if(id_relative_4.getVisibility() == View.GONE){
                    id_relative_4.setVisibility(View.VISIBLE);
                }
                id_item_invoice.setImageBitmap(RotateImage.decodeSampledBitmapFromResource(getContext(), getResources(), mImageFileLocation, 97, 90));
                id_addInvoice.setVisibility(View.GONE);
            }
        }else{
            resultCanceled = 1;
        }
        if (resultCanceled == 1){
            switch (requestCode){
                case 1880:
                    imageUpdate1 = 0;
                    break;
                case 1881:
                    imageUpdate2 = 0;
                    break;
                case 1882:
                    imageUpdate3 = 0;
                case 1883:
                    invoiceUpdate = 0;
                    break;
                default:
                    break;
            }
        }
    }
}
