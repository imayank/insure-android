package com.stampsure.stampsure.beans;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class PieAmountFormatter implements IValueFormatter {
    private DecimalFormat mFormat;
    public PieAmountFormatter() {
        mFormat = new DecimalFormat("###,###,##0.00"); // use two decimal
    }
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return "R " + mFormat.format(value);
    }
}
