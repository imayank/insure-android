package com.stampsure.stampsure.beans;

import java.util.Date;

public class UpdateItemDetails {

    private byte[] id_item_pic1;
    private byte[] id_item_pic2;
    private byte[] id_item_pic3;
    private byte[] id_invoice;
    private int id_items_number;
    private String id_asset_name;
    private String id_asset_desc;
    private int id_asset_value;
    private int id_asset_category;
    private int UserID;
    private String id_location;
    private Date id_asset_purchase_date;
    private int id_asset_age;

    public int getId_items_number() {
        return id_items_number;
    }

    public void setId_items_number(int id_items_number) {
        this.id_items_number = id_items_number;
    }

    public String getId_asset_name() {
        return id_asset_name;
    }

    public void setId_asset_name(String id_asset_name) {
        this.id_asset_name = id_asset_name;
    }

    public String getId_asset_desc() {
        return id_asset_desc;
    }

    public void setId_asset_desc(String id_asset_desc) {
        this.id_asset_desc = id_asset_desc;
    }

    public int getId_asset_value() {
        return id_asset_value;
    }

    public void setId_asset_value(int id_asset_value) {
        this.id_asset_value = id_asset_value;
    }

    public int getId_asset_category() {
        return id_asset_category;
    }

    public void setId_asset_category(int id_asset_category) {
        this.id_asset_category = id_asset_category;
    }

    public String getId_location() {
        return id_location;
    }

    public void setId_location(String id_location) {
        this.id_location = id_location;
    }

    public Date getId_asset_purchase_date() {
        return id_asset_purchase_date;
    }

    public void setId_asset_purchase_date(Date id_asset_purchase_date) {
        this.id_asset_purchase_date = id_asset_purchase_date;
    }

    public int getId_asset_age() {
        return id_asset_age;
    }

    public void setId_asset_age(int id_asset_age) {
        this.id_asset_age = id_asset_age;
    }

    public byte[] getId_item_pic1() {
        return id_item_pic1;
    }

    public void setId_item_pic1(byte[] id_item_pic1) {
        this.id_item_pic1 = id_item_pic1;
    }

    public byte[] getId_item_pic2() {
        return id_item_pic2;
    }

    public void setId_item_pic2(byte[] id_item_pic2) {
        this.id_item_pic2 = id_item_pic2;
    }

    public byte[] getId_item_pic3() {
        return id_item_pic3;
    }

    public void setId_item_pic3(byte[] id_item_pic3) {
        this.id_item_pic3 = id_item_pic3;
    }


    public byte[] getInvoice() {
        return id_item_pic3;
    }

    public void setInvoice(byte[] id_invoice) {
        this.id_invoice = id_invoice;
    }

    public byte[] getId_invoice() {
        return id_invoice;
    }

    public void setId_invoice(byte[] id_invoice) {
        this.id_invoice = id_invoice;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }
}
