package com.stampsure.stampsure.beans;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.app.AlertDialog;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.content.Context;
import android.content.DialogInterface;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;

import com.stampsure.stampsure.fragments.UpdateItemFragment;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter";
    private static final String DEFAULT_CURRENCY = "R";
    private List<AssetStampCart> list;
    private List<AssetStampCart> vList;
    private int UID;
    public Context context ;
    private Context vContext ;
    private int vUID ;

    public ItemListAdapter(int UID,List<AssetStampCart> list, Context context){
        this.context = context;
        this.list = list;
        this.UID = UID;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_layout,parent,false);
        ViewHolder holder = new ViewHolder(view, UID, context, list);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder. called."+ position);
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(context);
        Glide.with(context)
                .asBitmap()
                .load(list.get(position).getAsset().getImage1Path())
                .into(holder.circleImageView);

        holder.itemName.setText(list.get(position).getAsset().getAssetName());
        holder.itemCategory.setText(databaseHandler.getCategoryName(list.get(position).getAsset().getCategoryId()));
        holder.itemValue.setText(DEFAULT_CURRENCY +" "+list.get(position).getAsset().getBrandPrice().toString());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ViewHolder(View itemView, int UID, final Context context , final List<AssetStampCart> list){

            super(itemView);
            vContext = context;
            vUID = UID;
            vList = list;
            circleImageView = itemView.findViewById(R.id.id_image);
            itemCategory    = itemView.findViewById(R.id.id_item_category);
            itemName        = itemView.findViewById(R.id.id_item_name);
            itemValue       = itemView.findViewById(R.id.id_item_value);
       //     is_selected     = itemView.findViewById(R.id.is_selected);
            id_delete_item  = itemView.findViewById(R.id.id_delete_item);
            id_edit_item    = itemView.findViewById(R.id.id_edit_item);
            id_relative_layout = itemView.findViewById(R.id.id_relative_layout);

            id_delete_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(getAdapterPosition());
                }
            });

            id_edit_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    UpdateItemFragment updateItemFragment = new UpdateItemFragment();
                    Bundle input = new Bundle();
                    input.putSerializable("Asset",list.get(getAdapterPosition()).getAsset());
                    updateItemFragment.setArguments(input);
                    fragmentTransaction.replace(R.id.id_main_frame, updateItemFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
        }

        private void removeItem(final int position){
            Log.d(TAG, "removeItem: " + "position "+position + " id "+ vList.get(position).getAsset().getId() + " UID "+vUID);
            final AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(vContext);
            createOptionDialog.setTitle("Do you want to delete this item");
            createOptionDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(vContext);
                    databaseHandler.deleteAssetData(vList.get(position).getAsset().getId(),  vUID);
                    vList.remove(position);
                    notifyItemRemoved(position);

                    // Update Total Asset Value.
                    Double total_value = 0.00;
                    for (int i = 0; i < vList.size(); i++){
                        total_value = total_value + vList.get(i).getAsset().getBrandPrice();
                    }
                    TextView id_total_value = ((Activity)context).getWindow().getDecorView().findViewById(R.id.id_total_value);
                    id_total_value.setText(String.valueOf(total_value));

                }
            });

            createOptionDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            createOptionDialog.show();
        }
      //  CircleImageView circleImageView;
        ImageView circleImageView;
        TextView itemName;
        TextView itemCategory;
        TextView itemValue;
      //  ImageView is_selected;
        Button id_delete_item;
        Button id_edit_item;
        RelativeLayout id_relative_layout;
    }
}
