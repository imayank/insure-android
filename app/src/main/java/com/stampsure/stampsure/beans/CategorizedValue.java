package com.stampsure.stampsure.beans;

public class CategorizedValue {
    Double [] totalValue;
    Integer[] CategoryId;
    String [] Location;

    public Double[] getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double[] totalValue) {
        this.totalValue = totalValue;
    }

    public Integer[] getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Integer[] categoryId) {
        CategoryId = categoryId;
    }

    public String[] getLocation() {
        return Location;
    }

    public void setLocation(String[] location) {
        Location = location;
    }
}
