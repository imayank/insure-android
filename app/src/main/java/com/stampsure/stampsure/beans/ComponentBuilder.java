package com.stampsure.stampsure.beans;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import id.zelory.compressor.Compressor;

/**
 * Created by Mayank Tantuway on 05-10-2016.
 */
public class ComponentBuilder {

    public static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
    public static final String TAG = "ComponentBuilder";
    public static int actionResultCode = 0;
    private static String imagePath = "";
    /**
     *
     * @param datePicker
     * @return a java.util.Date
     */

    //public static ProgressDialog progressDialog;
    public static ProgressDialog progressDialog;
    public static AlertDialog alertDialog;
    public static Dialog pd;
    public static TextView cartCount;

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    //public static LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    // left top right bottom
    public static int[] defaultPaddingArray = {5, 5, 10, 5};

    public static RestTemplate getRestTemplate (){
        // Time Out Configuration : start
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        // httpRequestFactory.setConnectTimeout(ApplicationConstant.CONNECTION_TIMEOUT); // 2 minutes CONNECTION_TIMEOUT
        // httpRequestFactory.setReadTimeout(ApplicationConstant.READ_TIMEOUT);// 2 minutes READ_TIMEOUT
        RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
        // Time Out Configuration : end
        //TODO Always set timeout to
        // Normal Template without Timeout
        //RestTemplate restTemplate = new RestTemplate();
        HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();

        restTemplate.getMessageConverters().add(formHttpMessageConverter);
        restTemplate.getMessageConverters().add(stringHttpMessageConverter);
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
        restTemplate.getMessageConverters().add(new ResourceHttpMessageConverter());

        return restTemplate;
    }


    public static Button createNewButton(String textViewText, LinearLayout.LayoutParams layoutParams, int[] padding, boolean singleton,Context context) {
        Button newButton = new Button(context);
        newButton.setText(textViewText);
        newButton.setLayoutParams(layoutParams);
        newButton.setPadding(padding[0], padding[1], padding[2], padding[3]);
        newButton.setSingleLine(singleton);
        return newButton;
    }
    /*
        public static TextView createDataTextView(String textViewText, LinearLayout.LayoutParams layoutParams, int[] padding, Context context) {
            TextView newTextView = new TextView(context);
            newTextView.setText(textViewText);
            newTextView.setLayoutParams(layoutParams);
            newTextView.setPadding(padding[0], padding[1], padding[2], padding[3]);
            newTextView.setTextAppearance(context, R.style.dataCodeFont);
            return newTextView;
        }
    */
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }



    public static void hideProgressDialogue(){
        // Safe dismiss to avoid null pointer
        if(progressDialog!=null) {
            progressDialog.dismiss();
        }
    }


    public static void hideTransparentProgress(){
        pd.hide();
    }

    public static void setToolbarTextStyle(Toolbar toolbar){
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setSubtitleTextColor(Color.BLACK);
    }

    public static void setConnectOrNot(Context context,String testing){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("connect", testing).commit();
    }

    public static String getConnectOrNot(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString("connect","");
    }

/*
    public static void setDefaultAddress(Context fromContext, UserBean userBean){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fromContext);
        prefs.edit().putString("contactName", userBean.getUsername()).commit();
        prefs.edit().putString("contactAddress", userBean.getDescription()).commit();
        prefs.edit().putString("contactEmail", userBean.getEmail()).commit();
        String mobile1 = userBean.getMobile1();
        if(mobile1!=null && mobile1.length()>0) {
            prefs.edit().putLong("mobile1", Long.parseLong(mobile1)).commit();
        }
        String mobile2 = userBean.getMobile2();
        if(mobile2!=null && mobile2.length()>0) {
            prefs.edit().putLong("mobile2", Long.parseLong(mobile2)).commit();
        }
    }



    public static void cacheUserDetailsInPreferences(Context activityContext, UserProfile responseUserBean) {
        // Login Credentials
        ComponentBuilder.setIsUserLoggedInFromPref(activityContext,responseUserBean.getFName());
        ComponentBuilder.setLoggedInUserIdToPref(activityContext,responseUserBean.getEmailId());
        // Address History default to be login details
    }

    */

    public static String getCategoryName(Context context, int categoryId){
        LocalDatabaseHandler dbScreenUserHandler = new LocalDatabaseHandler(context);
      //  dbScreenUserHandler.getCategoryID(categorySelected);
        return "";
    }


    public static void setLoggedInUserNameToPref(Context fromContext, String loggedInUser){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(fromContext);
        pref.edit().putString("loggedInUser", loggedInUser).apply();
        Log.d(TAG,loggedInUser);
    }

    public static String getLoggedInUserNameToPref(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString("loggedInUser","");
    }

    public static void setLoggedInUserEmailFromActivity(Context context, String loggedInUserEmail){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString("loggedInUserEmail",loggedInUserEmail).apply();
        Log.d(TAG,"Set Value "+ loggedInUserEmail);
    }



    public static String getLoggedInUserEmailFromActivity(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString("loggedInUserEmail","");
    }


    public static void setCategoryFromActivity(Context context, Set<String> StringSet){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putStringSet("CategoryList",StringSet).apply();

        Log.d(TAG,"Set Value "+ StringSet);
    }

    public static Set<String> getCategoryFromActivity(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getStringSet("CategoryList",null);
    }


    public static void setUIDFromActivity(Context context, int UID){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt("UID",UID).apply();
    }

    public static int getUIDFromActivity(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("UID",0);
    }

    public static void setCategoryID(Context context, int categoryID){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt("CategoryID",categoryID).apply();
    }
    public static int getCategoryID(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("CategoryID",0);
    }

    public static void setLocation(Context context, String location){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("Location",location).apply();
    }

    public static String getLocation(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("Location","");
    }

    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        if (!(bitmap == null)) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            return stream.toByteArray();
        } else {
            return null;
        }
    }

    // ItemListFragment

    public static byte[] getCompressedFile (Context context, String imagePath){
        File file = new File(imagePath);
        try {
            Compressor compressedImage = new Compressor(context);
            Bitmap compressedBitmap =  compressedImage.compressToBitmap(file);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);

            return stream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int getAssetAge(Date purchaseDate){
        double msPerGregorianYear = 365.25 * 86400 * 1000;
        Date currentDate = new Date();
        double years = (currentDate.getTime() - purchaseDate.getTime()) / msPerGregorianYear;
        int yy = (int) years;
        int mm = (int) ((years - yy) * 12);
        int months = yy * 12 + mm;
        Log.d(TAG, "getAssetAge: " + months);
        return months;
    }
}
