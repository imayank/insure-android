package com.stampsure.stampsure.beans;

public class LoginProfile {
    private String emailId;
    private String logonPassword;

    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    public String getLogonPassword() {
        return logonPassword;
    }
    public void setLogonPassword(String logonPassword) {
        this.logonPassword = logonPassword;
    }
}
