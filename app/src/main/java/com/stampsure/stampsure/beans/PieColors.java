package com.stampsure.stampsure.beans;

import android.graphics.Color;

import java.util.ArrayList;

public class PieColors {
    public static ArrayList<Integer> getPieColors(){
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.argb(255, 115, 207, 237));
        colors.add(Color.argb(255, 172, 125, 234));
        colors.add(Color.argb(255, 222, 188, 122));
        colors.add(Color.argb(255, 199, 188, 103));
        colors.add(Color.argb(255, 228, 84, 137));
        colors.add(Color.argb(255, 166, 150, 156));
        colors.add(Color.argb(255, 244, 52, 124));
        colors.add(Color.argb(255, 108, 104, 105));
        colors.add(Color.argb(255, 42, 136, 237));
        colors.add(Color.argb(255, 191, 132, 127));
        colors.add(Color.argb(255, 155, 62, 54));
        colors.add(Color.argb(255, 133, 206, 104));
        colors.add(Color.argb(255, 70, 99, 58));
        colors.add(Color.argb(255, 78, 247, 8));
        colors.add(Color.argb(255, 210, 195, 21));
        colors.add(Color.argb(255, 112, 105, 30));
        colors.add(Color.argb(255, 156, 143, 4));
        colors.add(Color.argb(255, 99, 145, 87));

        return colors;
    }
}
