package com.stampsure.stampsure.beans;

import java.io.Serializable;
import java.util.Date;

public class Asset implements Serializable {

    private static final long serialVersionUID = 6L;

    private int id;

    private String assetName;

    private Integer userId;

    private Integer categoryId;

    private String brandDesc;

    private Double brandPrice;

    private byte[] image1;

    private byte[] image2;

    private byte[] image3;

    private byte[] invoiceImage;

    private String image1Path;

    private String image2Path;

    private String image3Path;

    private String invoicePath;

    private String assetLocation;

    private Integer ageOfAsset;

    private Integer quantity;

    private Date purchaseDate;

    private Date lastUpdated;

    private String status;

    private Double insuredValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrandDesc() {
        return brandDesc;
    }

    public void setBrandDesc(String brandDesc) {
        this.brandDesc = brandDesc;
    }

    public Double getBrandPrice() {
        return brandPrice;
    }

    public void setBrandPrice(Double brandPrice) {
        this.brandPrice = brandPrice;
    }

    public String getImage1Path() {
        return image1Path;
    }

    public void setImage1Path(String image1Path) {
        this.image1Path = image1Path;
    }

    public String getImage2Path() {
        return image2Path;
    }

    public void setImage2Path(String image2Path) {
        this.image2Path = image2Path;
    }

    public String getImage3Path() {
        return image3Path;
    }

    public void setImage3Path(String image3Path) {
        this.image3Path = image3Path;
    }

    public String getInvoicePath() {
        return invoicePath;
    }

    public void setInvoicePath(String invoicePath) {
        this.invoicePath = invoicePath;
    }

    public String getAssetLocation() {
        return assetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        this.assetLocation = assetLocation;
    }

    public Integer getAgeOfAsset() {
        return ageOfAsset;
    }

    public void setAgeOfAsset(Integer ageOfAsset) {
        this.ageOfAsset = ageOfAsset;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public byte[] getImage1() {
        return image1;
    }

    public void setImage1(byte[] image1) {
        this.image1 = image1;
    }

    public byte[] getImage2() {
        return image2;
    }

    public void setImage2(byte[] image2) {
        this.image2 = image2;
    }

    public byte[] getImage3() {
        return image3;
    }

    public void setImage3(byte[] image3) {
        this.image3 = image3;
    }

    public byte[] getInvoiceImage() {
        return invoiceImage;
    }

    public void setInvoiceImage(byte[] invoiceImage) {
        this.invoiceImage = invoiceImage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getInsuredValue() {
        return insuredValue;
    }

    public void setInsuredValue(Double insuredValue) {
        this.insuredValue = insuredValue;
    }
}