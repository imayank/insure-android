package com.stampsure.stampsure.beans;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class RotateImage {
    //  public static Bitmap rotateImage(Bitmap bitmap, String fileLocation){
    @TargetApi(24)
    public static Bitmap rotateImage(Context context, Bitmap bitmap, Uri fileLocation){
        ExifInterface exifInterface = null;
        try{
           // Uri uri = Uri.fromFile(new File(fileLocation));
            Uri uri = fileLocation;
            InputStream in =  context.getApplicationContext().getContentResolver().openInputStream(uri);
            exifInterface = new ExifInterface(in);
        }catch (Exception e){
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(exifInterface.TAG_ORIENTATION,exifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap image = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
        Bitmap resized = ThumbnailUtils.extractThumbnail(image,image.getWidth(),image.getHeight()/2);
        return resized;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d("inSampleSize",String.valueOf( inSampleSize));
        return inSampleSize;
    }
    @TargetApi(24)
    public static Bitmap decodeSampledBitmapFromResource(Context context,Resources res, String imagePath,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        // BitmapFactory.decodeResource(res, resId, options);
        BitmapFactory.decodeFile(imagePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        ExifInterface exifInterface = null;
        try{
            Uri uri = Uri.fromFile(new File(imagePath));
            InputStream in =  context.getApplicationContext().getContentResolver().openInputStream(uri);
            exifInterface = new ExifInterface(in);
        }catch (Exception e){
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(exifInterface.TAG_ORIENTATION,exifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap bitmapImage = BitmapFactory.decodeFile(imagePath, options);
        Bitmap image = Bitmap.createBitmap(bitmapImage,0,0,bitmapImage.getWidth() / 2 , bitmapImage.getHeight() ,matrix,true );
        return image;
    }
}
