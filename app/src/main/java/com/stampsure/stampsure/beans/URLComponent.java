package com.stampsure.stampsure.beans;

public class URLComponent {

  // DashBoardCategory
    // DashboardLocation
    public static final String getAssetURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/asset/getUserAssets";

    // Register
    public static final String addProfileURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/addUserProfile";

    // AddItemFragment
    // ItemListFragment
    public static final String addAssetURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/asset/captureAssets";

    // Register
    // StampActivity
    public static final String getCategoryURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/categories";

    // LoginActivity
    public static final String loginURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/loginUserProfile";

    public static final String deleteAssetByAssetId = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/asset/deleteAssetById";
    public static final String updateAsset = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/asset/updateAsset";

    public static final String forgotPassword = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/forgotPassword";
    public static final String changePasswordURL = "http://ec2-18-218-148-47.us-east-2.compute.amazonaws.com:8090/profile/changePassword";


/*
    public static final String getAssetURL = "http://10.0.75.1:8090/asset/getUserAssets";
    public static final String addProfileURL = "http://10.0.75.1:8090/profile/addUserProfile";
    public static final String addAssetURL = "http://10.0.75.1:8090/asset/captureAssets";
    public static final String getCategoryURL = "http://10.0.75.1:8090/profile/categories";
    public static final String loginURL = "http://10.0.75.1:8090/profile/loginUserProfile";
    public static final String deleteAssetByAssetId = "http://10.0.75.1:8090/asset/deleteAssetById";
    public static final String updateAsset = "http://10.0.75.1:8090//asset/updateAsset";

    public static final String forgotPassword = "http://10.0.75.1:8090/profile/forgotPassword";

*/

}
