package com.stampsure.stampsure.beans;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stampsure.stampsure.LocalDatabase.LocalDatabaseHandler;
import com.stampsure.stampsure.R;
import com.stampsure.stampsure.fragments.UpdateServerItemFragment;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemServerListAdapter extends RecyclerView.Adapter<ItemServerListAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter";
    private static final String DEFAULT_CURRENCY = "R";
    private List<AssetStampCart> list;
    private List<AssetStampCart> vList;
    private int UID;
    public Context context ;
    public Context vContext ;
    public int vUID ;


    public ItemServerListAdapter(int UID, List<AssetStampCart> list, Context context){
        this.context = context;
        this.list = list;
        this.UID = UID;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_layout,parent,false);
        ViewHolder holder = new ViewHolder(view, UID, context, list);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder. called."+ position);
        LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(context);

        Glide.with(context)
                .asBitmap()
                .load(list.get(position).getAsset().getImage1())
                .into(holder.circleImageView);

        holder.itemName.setText(list.get(position).getAsset().getAssetName());
        holder.itemCategory.setText(databaseHandler.getCategoryName(list.get(position).getAsset().getCategoryId()));
        holder.itemValue.setText( DEFAULT_CURRENCY+ " " +list.get(position).getAsset().getBrandPrice().toString());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ViewHolder(View itemView, int UID, Context context ,List<AssetStampCart> list){
            super(itemView);
            vContext = context;
            vUID = UID;
            vList = list;
            circleImageView = itemView.findViewById(R.id.id_image);
            itemCategory    = itemView.findViewById(R.id.id_item_category);
            itemName        = itemView.findViewById(R.id.id_item_name);
            itemValue       = itemView.findViewById(R.id.id_item_value);
          //  is_selected     = itemView.findViewById(R.id.is_selected);
            id_delete_item  = itemView.findViewById(R.id.id_delete_item);
            id_edit_item    = itemView.findViewById(R.id.id_edit_item);
            id_relative_layout = itemView.findViewById(R.id.id_relative_layout);

            id_delete_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(getAdapterPosition());
                }
            });

            id_edit_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FragmentManager fragmentManager = ((FragmentActivity)vContext).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    UpdateServerItemFragment updateServerItemFragment = new UpdateServerItemFragment();
                    Bundle input = new Bundle();
                    input.putSerializable("AssetId",vList.get(getAdapterPosition()).getAsset().getId());
                    updateServerItemFragment.setArguments(input);
                    fragmentTransaction.replace(R.id.id_main_frame, updateServerItemFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
        }

        private void removeItem(final int position){

            final AlertDialog.Builder createOptionDialog = new AlertDialog.Builder(vContext);
            createOptionDialog.setTitle("Do you want to delete this item");
            createOptionDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LocalDatabaseHandler databaseHandler = new LocalDatabaseHandler(vContext);
                    databaseHandler.deleteAssetServerData(vList.get(position).getAsset().getId(),  vUID);
                    new HttpDeleteItem().execute(vList.get(position).getAsset().getId());
                    vList.remove(position);
                    notifyItemRemoved(position);

                    // Update Total Asset Value.
                    Double total_value = 0.00;
                    for (int i = 0; i < vList.size(); i++){
                        total_value = total_value + vList.get(i).getAsset().getBrandPrice();
                    }
                    TextView id_total_value = ((Activity)vContext).getWindow().getDecorView().findViewById(R.id.id_total_value_categoryView);
                    id_total_value.setText(String.valueOf(total_value));
                }
            });

            createOptionDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            createOptionDialog.show();
        }

      //  CircleImageView circleImageView;
        ImageView circleImageView;
        TextView itemName;
        TextView itemCategory;
        TextView itemValue;
    //    ImageView is_selected;
        Button id_delete_item;
        Button id_edit_item;
        RelativeLayout id_relative_layout;
    }
/*
    private class HttpDeleteItem extends AsyncTask<Integer,Void,Void>{

        @Override
        protected Void doInBackground(Integer... assetID) {
            try{
                String deleteAsset = URLComponent.deleteAssetByAssetId + "/" + assetID[0];
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                restTemplate.delete(deleteAsset);
            }catch(Exception e){
                Toast.makeText(context,"Item not deleted",Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }
*/

    private class HttpDeleteItem extends AsyncTask<Integer,Void,ResponseEntity<String> >{
        ResponseEntity<String> response = null;
        @Override
        protected ResponseEntity<String>  doInBackground(Integer... assetID) {
            try{
                String deleteAsset = URLComponent.deleteAssetByAssetId + "/" + assetID[0];
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                //    restTemplate.delete(deleteAsset);
                response = restTemplate.exchange(deleteAsset, HttpMethod.DELETE,null,String.class);
            }catch(Exception e){
                Toast.makeText(context,"Item not deleted",Toast.LENGTH_SHORT).show();
            }
            return response;
        }

        @Override
        protected void onPostExecute(ResponseEntity<String> stringResponseEntity) {
            super.onPostExecute(stringResponseEntity);
            if(stringResponseEntity.getBody().equals("true")){
                Toast.makeText(context,"Item deleted successfully",Toast.LENGTH_SHORT).show();
            }
        }
    }


}
