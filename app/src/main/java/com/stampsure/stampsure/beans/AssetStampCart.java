package com.stampsure.stampsure.beans;

public class AssetStampCart {
    public boolean isSelected;
    public Asset asset;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
}
