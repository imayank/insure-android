package com.stampsure.stampsure.beans;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;


public class LabelFormatter implements IAxisValueFormatter{
    private final String[] mLabels;

    public LabelFormatter(String[] mLabels) {
        this.mLabels = mLabels;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mLabels[(int) value];
    }

}
