package com.stampsure.stampsure;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.stampsure.stampsure.beans.ComponentBuilder;
import com.stampsure.stampsure.beans.URLComponent;
import com.stampsure.stampsure.beans.UserProfile;

import org.springframework.web.client.RestTemplate;
import java.lang.ref.WeakReference;

public class ForgotPassword extends AppCompatActivity {
    private EditText id_email_forgot_password;
    Button id_button_reset_password;
    private static final String TAG = "ForgotPassword";
    private String email_post;
    private TextInputLayout email_input_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = findViewById(R.id.id_action_bar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle(R.string.reset_psw);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id_email_forgot_password = findViewById(R.id.id_email_forgot_psw);
        id_button_reset_password = findViewById(R.id.id_button_reset_password);

        id_button_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(id_button_reset_password.getWindowToken(), 0);

                UserProfile profile = new UserProfile();
                profile.setEmailId(email_post);
                email_post = id_email_forgot_password.getText().toString().trim();
                profile.setEmailId(email_post);
                new HttpForgotPasswordActivity(ForgotPassword.this).execute(profile);
            }
        });
    }

    private static class HttpForgotPasswordActivity extends AsyncTask<UserProfile, Void, Boolean> {
        private WeakReference<ForgotPassword> weakReference;
        private ProgressDialog progressDialog;
        HttpForgotPasswordActivity(ForgotPassword context){
            weakReference = new WeakReference<>(context);
        }

        @Override
        protected Boolean doInBackground(UserProfile... params) {
            Boolean response = false;
            try {

                final String resetPasswordUrl = URLComponent.forgotPassword;
                RestTemplate restTemplate = ComponentBuilder.getRestTemplate();
                response = restTemplate.postForObject(resetPasswordUrl, params[0], Boolean.class);
            } catch (Exception e) {
                ComponentBuilder.hideProgressDialogue();
                Log.e(TAG, e.getMessage(), e);
            }
            return response;
        }

        @Override
        protected void onPreExecute() {
            ForgotPassword activity = weakReference.get();
            // ComponentBuilder.showProgressDialogue(getApplicationContext(),"Uploading Images", "Please wait for a moment.");
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Processing..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean response) {
            ForgotPassword activity = weakReference.get();
            TextView id_forgot_psw_post_msg   = activity.findViewById(R.id.id_forgot_psw_post_msg);

            if (activity.isFinishing()) return;
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(response){
                activity.findViewById(R.id.email_input_layout).setVisibility(View.GONE);
                activity.findViewById(R.id.text_message).setVisibility(View.GONE);
                activity.id_button_reset_password.setVisibility(View.GONE);
                id_forgot_psw_post_msg.setVisibility(View.VISIBLE);
            }else{
                id_forgot_psw_post_msg.setVisibility(View.VISIBLE);
                id_forgot_psw_post_msg.setText(R.string.process_failed);
                id_forgot_psw_post_msg.setTextColor(activity.getResources().getColor(R.color.alertColor));
            }
        }
    }
}
